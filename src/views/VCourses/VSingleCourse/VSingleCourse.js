import pagination from "@/mixins/pagination";
import payment from '@/mixins/payment';
import StarRating from 'vue-star-rating';
import teacherOnline from '@/mixins/teacherOnline';
import {mapMutations} from "vuex";
export default {
  name: 'VSingleCourse',
  mixins: [pagination, payment, teacherOnline],
  components: { StarRating },
  data() {
    return {
      isActive: false,
      propertyName: 0,
      course: {
        desc: '',
        image: '',
        language: '',
        lessonCount: '',
        name: '',
        oldCost: null,
        cost: '',
        rating: 0,
        text: '',
        hasSubscription: false,
        isFavourite: false,
        isCombo: false,
        includes: [],
        author: {
          avatar: '',
          name: '',
          desc: ''
        },
      },
      chapters: [],
      reviews: [],
      isLoad: false,
      courseId: null,
      reviewStr: '',
      rating: 0,
    }
  },
  created() {
    if (!this.$isServer) {
      this._scrollListener()
      window.addEventListener('scroll', this._scrollListener)
    }
  },
  mounted() {
    this.courseId = this.$route.params.id;
    this.fetchCourseById();
    this.fetchReviewsById()
    this.fetchContentsById()
  },
  beforeDestroy() {
    window.removeEventListener('scroll', this._scrollListener)
  },
  watch: {
    propertyName() {
      this.$refs.leftOuterMenu.style.height = this.$refs.leftInnerMenu.clientHeight+'px';
      let aboutHeight = document.getElementById('courseAbout').getBoundingClientRect().top;
      let contentHeight = document.getElementById('courseContent').getBoundingClientRect().top;
      let authorHeight = document.getElementById('courseAuthor').getBoundingClientRect().top;
      let feedbackHeight = document.getElementById('courseComment').getBoundingClientRect().top;
      if (aboutHeight <= 70) {
        this.removeActiveClass();
        this.$refs.leftMenuAbout.classList.add("active");
      }
      if (contentHeight <= 100) {
        this.removeActiveClass();
        this.$refs.leftMenuContent.classList.add("active");
      }
      if (authorHeight <= 100) {
        this.removeActiveClass();
        this.$refs.leftMenuAuthor.classList.add("active");
      }
      if (feedbackHeight <= 100) {
        this.removeActiveClass();
        this.$refs.leftMenuFeedback.classList.add("active");
      }
    }
  },
  methods: {
    ...mapMutations([
      'setPurchasedCourse'
    ]),
    fetchCourseById() {
      this.$http({
        url: this.$getAPI('course.getCourseById', {
          id: this.courseId
        })
      }).then((res) => {
        this.course = res;
        this.setEventDataLayer('select_item');
        this.setEventDataLayer('view_item');
      })
    },
    fetchReviewsById() {
      this.$http({
        url: this.$getAPI('course.listReviews', { id: this.courseId }),
        params: {
          page: this.currentPage
        }
      }).then((res) => {
        if (!res.error) {
          this.reviews = this.reviews.concat(res.rows);
          this.currentPage = res.currentPage;
          this.lastPage = res.lastPage;
          this.hasNext = res.hasNext;
        }
        this.isLoad = false;
      })
    },
    fetchContentsById() {
      this.$http({
        url: this.$getAPI('course.contents', {
          id: this.courseId
        })
      }).then((res) => {
        this.chapters = res.chapters;
      })
    },
    fetchMoreData() {
      this.isLoad = true;
      this.currentPage += 1;
      this.fetchReviewsById(this.courseId);
    },
    dateCreateReview(date) {
      return new Date(date).toLocaleString();
    },
    _scrollListener() {
      this.propertyName = Math.round(window.pageYOffset);
    },
    removeActiveClass() {
      let leftMenu = this.$refs.leftInnerMenu.getElementsByTagName('a');
      for (let i = 0; i<leftMenu.length; i++) {
        leftMenu[i].classList.remove("active");
      }
    },
    handleLike() {
      this.$http({
        url: this.$getAPI('favorite.deleteOrAddFavorite', {
          subject_id: this.courseId
        }),
        method: 'POST'
      }).then((res) => {
        if (!res.error) {
          this.$notify({
            type: 'success',
            title: 'Успешно',
            text: 'Успешно!',
          });
        }
      });
    },
    handleSubscribe() {
      this.setEventDataLayer('select_item');
      this.setEventDataLayer('view_item');
      this.setEventDataLayer('add_to_cart');
      this.$vGlModal.show({
        modalName: 'v-booking-modal',
        modalTitle: 'Оформления заказа',
        modalWidth: 770,
        component: 'v-booking-modal',
        componentProps: {
          id: Number(this.courseId),
          handleGoToPayment: (object) => {
            const { payment_type, discipline } = object;
            const api = 'payment.payCourse';
            const params = {
              itemId: this.courseId,
              subject_id: discipline ? discipline.id : null
            }
            this.setEventDataLayer('begin_checkout');
            this.setEventPaymentDataLayer('add_payment_info', payment_type);
            this.setPurchasedCourse(this.course);
            if (payment_type === 'beeline') {
              this.showBeelineModal({ api, ...params, ...object });
            } else {
              this.handlePay({ api, ...params, ...object  });
            }
          }
        }
      })
    },
    handleSendReview() {
      this.$http({
        url: this.$getAPI('course.addReview', {
          id: this.courseId
        }),
        method: 'POST',
        data: {
          review_text: this.reviewStr,
          rating: this.rating
        }
      }).then((res) => {
        if (!res.error) {
          this.$notify({
            type: 'success',
            title: '',
            text: res.message
          });
          this.reviewStr = '';
          this.rating = 0;
          this.reviews = [];
          this.fetchReviewsById();
        }
      })
    },
    handleOpenLesson(item, index, array) {
      item.active = !item.active;
      this.$set(array, index, item)
      console.log('lesson', item)
    },
    setEventDataLayer(type) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: type,
        ecommerce: {
          items: [{
            item_name: this.course.name,
            item_id: this.course.id,
            price: this.course.price_string,
            currency: 'Тенге',
            item_brand: "Курсы",
            item_category: "Курсы",
            index: 1,
            quantity: 1
          }]
        }
      });
    },
    setEventPaymentDataLayer(type, paymentType) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: type,
        payment_type: paymentType,
        ecommerce: {
          items: [{
            item_name: this.course.name,
            item_id: this.course.id,
            price: this.course.price_string,
            currency: 'Тенге',
            item_brand: "Курсы",
            item_category: "Курсы",
            index: 1,
            quantity: 1
          }]
        }
      });
    },
    chapterClass(chapter) {
      return [
        {'access-open passed-lesson': chapter.doneLessonsCount === chapter.lessonsCount},
        {'active': chapter.active }
      ]
    },
    lessonClass(lesson) {
      return [
        {'overall-task access-open passed-lesson': lesson.isAccept},
        {'active': lesson.active }
      ]
    }
  }
}
