export default {
  data() {
    return {
      validation: {},
    }
  },

  computed: {
    hasValidationFail() {
      const { validation } = this;
      return (field) => Array.isArray(validation[field]) && validation[field].length > 0;
    },
  },

  methods: {
    addValidationError(field, value) {
      if (!Array.isArray(this.validation[field])) {
        this.validation[field] = [];
      }

      this.validation = {
        ...this.validation,
        [field]: [...value],
      };
    },

    getValidationMessage(field) {
      const {
        validation,
        hasValidationFail,
      } = this;
      return (hasValidationFail(field) && validation[field][0]) || '';
    },

    flashValidationFail(field) {
      if (this.validation[field]) {
        const obj = {};
        Object.keys(this.validation).forEach((item) => {
          if (field !== item) {
            obj[item] = this.validation[item];
          }
        });
        this.validation = obj;
      }
    },
  }
}