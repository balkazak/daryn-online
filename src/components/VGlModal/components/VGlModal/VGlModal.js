export default {
  name: 'VGlModal',

  props: {
    title: String,
    locked: Boolean,
    name: String,
    classes: String,
    zIndex: Number,
    width: Number,
  },

  data() {
    return {
      prevTargetDataset: null,
      loading: false,
      titleLocal: this.title,
      params: {},
    };
  },

  mounted() {
    this.$eventHub.$emit(`v-gl-modal--${this.name}-mounted`);
    this.$eventHub.$on(`v-gl-modal--${this.name}-update`, this.handleUpdate);
  },

  beforeDestroy() {
    this.$eventHub.$emit(`v-gl-modal--${this.name}-destroy`);
    this.$eventHub.$off(`v-gl-modal--${this.name}-update`);
  },

  methods: {
    glParentModal() {
      return true;
    },

    glClose() {
      if (!this.loading) {
        this.$vGlModal.hide(this.name);
      }
    },

    glChangeTitle(title) {
      this.titleLocal = title;
    },

    glLoading(bool) {
      this.loading = bool;
    },

    handleUpdate(obj) {
      this.params = obj;
    },

    handleClose(e) {
      if (e.type === 'mousedown') {
        this.prevTargetDataset = typeof e.target.dataset.vGlModal !== 'undefined' && !this.locked;
      } else if (e.type === 'mouseup') {
        if (typeof e.target.dataset.vGlModal !== 'undefined'
            && !this.locked
            && this.prevTargetDataset) {
          this.glClose();
        }
      }
    },
  },
};
