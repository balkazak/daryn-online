import errorHandler from "../helpers/errorHandler";
import render from '@/helpers/renderRouterView';


export default [
  {
    path: 'school-test',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VSchoolTest',
        component: () => import('../views/VSchoolTest').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/:userTestId',
        name: 'VSchoolTestQuestion',
        component: () => import('../views/VSchoolTest/VSchoolTestQuestion').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'result/:userTestId',
        name: 'VSchoolTestResult',
        component: () => import('../views/VSchoolTest/VSchoolTestResult').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
    ]
  }
]