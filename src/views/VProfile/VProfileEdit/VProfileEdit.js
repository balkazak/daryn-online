import VSelect from "../../../components/VCustomSelect";
import VInput from "../../../components/VCustomInput";

export default {
  name: 'VProfileEdit',

  components: {
    VSelect,
    VInput
  },

  data() {
    return {
      form: {
        firstName: '',
        lastName: '',
        email: null,
        phone: '',
        grade: null,
        school: null,
        region: null,
        is_schoolchild: null,
        district: null
      },
      regionList: [],
      schoolList: [],
      cityList: [],
      gradeList: [],
      schoolIds: [
        {id: 0, name: 'Оқушы емеспiн'},
        {id: 1, name: 'Оқушы'}
      ]
    }
  },

  async created() {
    this.$store.commit('setLoading', true);
    await this.getDictionary();
    await this.getFetchData();
    this.$store.commit('setLoading', false);
  },

  methods: {
    async getFetchData() {
      const profile = await this.$http({ url: this.$getAPI('profile.getProfile') });
      this.setDataForm(profile);
      await this.getCitiesByRegionId(profile?.region?.id);
    },
    async getDictionary() {
      try {
        const res = await this.$http({ url: this.$getAPI('dictionary.regions') });
        this.regionList = res?.regions;
        this.gradeList = [...Array(12).keys()];
      } catch (error) {
        console.error(error);
      }
    },

    async getCitiesByRegionId(id) {
      const res = await this.$http({
        url: this.$getAPI('dictionary.regions'),
        params: {
          parent_id: id
        }
      });
      if (!res.error) {
        this.cityList = res?.regions;
      }
    },

    async getSchoolsByRegionId(id) {
      const res = await this.$http({
        url: this.$getAPI('dictionary.schools'),
        params: {
          region_id: id
        }
      });
      if (!res.error) {
        this.schoolList = res?.schools;
      }
    },

    setDataForm(profile) {
      this.form = {
        ...profile,
        is_schoolchild: this.schoolIds.find(el => el.id === profile.isPupil)
      }
    },

    onChangeRegion(value) {
      this.form.district = null;
      this.cityList = [];
      this.schoolList = [];
      this.form.school = null;
      if (value?.childCount) {
        this.getCitiesByRegionId(value.id);
      } else {
        this.getSchoolsByRegionId(value.id);
      }
    },

    onChangeDistrict(value) {
      this.schoolList = [];
      this.form.school = null;
      this.getSchoolsByRegionId(value?.id);
    },

    async save() {
      const data = {
        first_name: this.form.firstName,
        last_name: this.form.lastName,
        email: this.form.email,
        phone: this.form.phone,
        grade: this.form.is_schoolchild?.id ? this.form.grade : null,
        region_id: this.form.district?.id || this.form.region?.id,
        school_id: this.form.school?.id,
        is_schoolchild: this.form.is_schoolchild?.id,
      }
      this.$store.commit('setLoading', true);

      const res = await this.$http({
        url: this.$getAPI('profile.saveProfile'),
        method: 'PUT',
        data: {
          ...data
        }
      });

      if (!res.error) {
        this.$notify({
          type: 'success',
          title: 'Успешно',
          text: res?.message,
          duration: 3000,
        });
      }
      this.$store.commit('setLoading', false);
    }
  }
}
