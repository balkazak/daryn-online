export default {
  name: 'VResultAttestation',

  data() {
    return {
      results: [],
      percent: '',
      text: ''
    }
  },

  async created() {
    await this.getFetchResult();
  },

  methods: {
    async getFetchResult() {
      const userAttestationId = this.$route.params?.id;
      try {
        this.$store.commit('setLoading', true)
        const res = await this.$http({
          url: this.$getAPI('attestation.result', {
            userAttestationId
          })
        });

        this.results = res.results;
        this.percent = res.results.percent;
        this.text = res.results.text;
      } finally {
        this.$store.commit('setLoading', false)
      }
    }
  }
}
