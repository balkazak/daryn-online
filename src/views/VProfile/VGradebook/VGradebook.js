import VPagination from '../../../components/VPagination'
import pagination from "@/mixins/pagination";
import IconCheck from "../../../components/icons/components/IconCheck";
import IconClose from "../../../components/icons/components/IconClose";
import IconBase from "../../../components/icons/IconBase";
import Multiselect from 'vue-multiselect'
export default {
  name: 'VGradebook',

  mixins: [pagination],

  components: {
    VPagination,
    IconBase,
    IconCheck,
    IconClose,
    Multiselect
  },

  data() {
    return {
      subject: '',
      allData: [],
      lastPage: 0,
      currentPage: 0,
      hasNext: false
    }
  },

  mounted() {
    this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setSpinnerLoader', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('gradeBook.gradeBook'),
          params: {
            page: this.currentPage,
          }
        });
        this.allData = res?.rows;
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
        this.hasNext = res?.hasNext;
        } finally {
        this.$store.commit('setSpinnerLoader', false);
      }
    },

    async changePage(page) {
      this.currentPage = page;
      await this.fetchData()
    },

    getClasses(item) {
      if (item.mark > 3) return 'success'
      else return 'danger'
    }
  }
}
