import errorHandler from "../helpers/errorHandler";
import render from "../helpers/renderRouterView";

export default [
  {
    path: 'free',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VFreeProduct',
        component: () => import('../views/VFreeProduct').catch(errorHandler),
        meta: {
          authorization: false,
        },
      },
    ]
  }
]
