import errorHandler from "../helpers/errorHandler";
import render from "../helpers/renderRouterView";

export default [
    {
        path: 'belbin',
        component: {
            render
        },
        children: [
            {
                path: '',
                name: 'VBelbin',
                component: () => import('../views/VBelbin').catch(errorHandler),
                meta: {
                    authorization: true,
                },
            },
            {
                path: 'result',
                name: 'VBelbinResult',
                component: () => import('../views/VBelbin/VBelbinResult').catch(errorHandler),
                meta: {
                    authorization: true,
                },
            },
        ]
    }
]
