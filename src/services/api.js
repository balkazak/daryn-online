import _ from 'lodash';

const uri = {
  auth: {
    login: '/api/auth/login',
    profile: '/api/user-info',
    logout: '/api/auth/logout',
    confirmEmail: '/api/email/verify/:userId/:hash',
    refreshToken: '/api/auth/refresh',
    loginWithToken: '/api/auth/login-with-token',
    register: '/api/auth/register',
    userRole: '/api/user-role'
  },
  menus: {
    menu: '/api/menus',
    getMenuById: '/api/menus/:url',
    info: '/api/infos'
  },
  article: {
    articles: '/api/articles',
    getArticleById: '/api/articles/:id',
    remove: '/api/articles/:id',
    like: '/api/articles/:id/like',
    edit: '/api/articles/:id',
    degreeDict: '/api/degrees',
    categoryDict: '/api/article/categories',
    myArticle: '/api/profile/articles',
    getCertificateById: '/api/articles/:id/certificate',
    payCertificate: '/api/articles/pay-certificate'
  },
  tutors: {
    tutors: '/api/tutors',
    singleTutor: '/api/tutors/:id',
    subjectDict: '/api/objects',
    languageDict: '/api/languages',
    regionDict: '/api/regions'
  },
  course: {
    myCourses: '/api/profile/subjects',
    allCourses: '/api/subjects',
    referralLink: '/api/profile/referral-link',
    courseFilter: '/api/subjects/filters',
    addToBasket: '/api/subjects/:id/basket',
    getCourseById: '/api/course/:id',
    listReviews: '/api/course/:id/reviews',
    contents: '/api/course/:id/chapters',
    getInfo: '/api/course/:id/get-info',
    addReview: '/api/course/:id/reviews',
    lessonById: '/api/lesson/:id',
    getQuestions: '/api/:id'
  },
  call: {
    historyCall: '/api/profile/calls'
  },
  favorite: {
    favorites: '/api/profile/favorites',
    deleteOrAddFavorite: '/api/subjects/:subject_id/favorite',
  },
  transaction: {
    transactions: '/api/profile/transactions'
  },
  reward: {
    rewards: '/api/profile/awards'
  },
  dictionary: {
    regions: '/api/regions',
    grades: '/api/grades',
    schools: '/api/schools',
    categories: '/api/article/categories',
    degrees: '/api/degrees',
    infoFiles: '/api/info-files',
    languages: '/api/languages',
    objects: '/api/objects'
  },
  profile: {
    saveProfile: '/api/profile',
    getProfile: '/api/profile',
    purse: '/api/profile/refill-purse'
  },
  common: {
    translates: '/api/translates'
  },
  ent: {
    objects: '/api/ent/objects',
    startEnt: '/api/ent/:id',
    getQuestions: '/api/ent/:id',
    sendAnswer: '/api/ent/:entId/questions/:questionId',
    finishEnt: '/api/ent/:id/finish',
    resultEnt: '/api/ent/:id/result',
    getListTest: '/api/ent',
    getId: '/api/ent/:id/get-id',
    check: '/api/ent/:id/check '
  },
  olympiad: {
    olympiads: '/api/olympiads',
    getOlympiad: '/api/olympiads/:id',
    rating: '/api/olympiads/rating',
    getQuestions: '/api/olympiad/:olympiadId',
    requests: '/api/requests',
    getId: '/api/olympiad/:id/get-id',
    sendAnswer: '/api/olympiad/:olympiadId/answer/:questionId',
    finish: '/api/olympiad/:olympiadId/finish',
    result: '/api/olympiad/:olympiadId/result',
    getCertificateById: '/api/olympiad/:olympiadId/certificate',
    getDiplomaById: '/api/olympiad/:olympiadId/diploma'
  },
  task: {
    tasks: '/api/profile/test-teacher',
    getId: '/api/profile/test-teacher/:id/start',
    getQuestions: '/api/profile/test-teacher/:testId',
    sendAnswer: '/api/profile/test-teacher/user_test_teacher_id/answer/question_id',
    finish: '/api/profile/test-teacher/user_test_teacher_id/result'
  },
  payment: {
    payBeeline: '/api/beeline/confirm-payment',
    payCertificate: '/api/articles/:id/pay-certificate',
    payOlympiad: '/api/olympiad/:id',
    payEnt: '/api/ent/:id/pay',
    resend: '/api/beeline/resend-code',
    payAttestation: '/api/attestation/:id',
    getPaymentInfo: '/api/get-payment-info',
    getEntityKinds: '/api/entity-kinds',
    payTrialTest: '/api/special-tests/:id',
    payCourse: '/api/course/:id',
    check: '/api/payment/check'
  },
  analysis: {
    allSpecialities: '/api/speciality',
    probability: '/api/speciality/probability',
    specialityDetail: '/api:url',
    universities: '/api/universities',
    getPlan: '/api/speciality/student-plan'
  },
  belbin: {
    getQuestions: '/api/belbin',
  },
  attestation: {
    getLessons: '/api/attestation',
    getQuestions: '/api/attestation/:id',
    getId: '/api/attestation/:id/get-id',
    sendAnswer: '/api/attestation/:userAttestationId/questions/:questionId',
    finish: '/api/attestation/:userAttestationId/finish',
    result: '/api/attestation/:userAttestationId/result',
  },
  trial: {
    getTrialTests: '/api/special-tests',
    getRules: '/api/special-tests/rule/:id',
    getId: '/api/special-tests/:id/get-id',
    getQuestions: '/api/special-tests/:trialTestId',
    sendAnswer: '/api/special-tests/:trialTestId/questions/:questionId',
    finish: '/api/special-tests/:trialTestId/finish',
    result: '/api/special-tests/:trialTestId/result'
  },
  schools: {
    getSchools: '/api/analysis-schools',
    categories: '/api/analysis-schools/categories',
    getSingleSchool: '/api/analysis-schools/:id',
  },
  faq: {
    getQuestions: '/api/faqs',
    vote: '/api/faqs/vote'
  },
  schoolTest: {
    schoolTests: '/api/tests',
    getId: '/api/tests/:id/start',
    getQuestions: '/api/tests/:userTestId',
    sendAnswer: '/api/tests/:userTestId/answer/:questionId',
    finish: '/api/tests/:userTestId/finish',
    result: '/api/tests/:userTestId/result'
  },
  home: {
    teachers: '/api/home/for-teachers',
    parents: '/api/home/for-parents',
    schools: '/api/home/for-schools',
    students: '/api/home/school-students',
    home: '/api/home',
    search: '/api/search/:id'
  },
  teacherOnline: {
    teacherOnline: '/api/teacher-online',
    getTeacherTime: '/api-teacher.daryn.online/api/v1/balance/calculate-duration'
  },
  banner: {
    banners: '/api/banner',
  },
  testResult: {
    olympiad: '/api/profile/test-results/olympiad',
    ent: '/api/profile/test-results/ent',
    special: '/api/profile/test-results/special-test',
    attestation: '/api/profile/test-results/attestation'
  },
  gradeBook: {
    gradeBook: '/api/markbook'
  },
  book: {
    books: '/api/books',
    bookDetail: '/api/books/:url'
  },
  schedule: {
    schedule: '/api/schedule'
  },
}
export default (uriName = '', params = {}) => {
  let uriStr = _.get(uri, uriName) || '';

  if (uriStr !== '') {
    if (_.isObject(params)) {
      _.forEach(params, (value, key) => {
        uriStr = uriStr.replace(`:${key}`, value);
      });
    }
  }

  return uriStr;
};
