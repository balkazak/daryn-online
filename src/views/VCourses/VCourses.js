import VPagination from '../../components/VPagination';
import pagination from "@/mixins/pagination";
import IconBase from "@/components/icons/IconBase";
import IconCourseList from "@/components/icons/components/IconCourseList";
import IconCourseGrid from "@/components/icons/components/IconCourseGrid";
import IconLessonCount from "@/components/icons/components/IconLessonCount";
import IconLanguage from "@/components/icons/components/IconLanguage";
import IconSubject from "@/components/icons/components/IconSubject";
import VCourseCard from "./components/VCourseCard";


function initialFilterParams (){
  return {
    type_id: null,
    category_id: null,
    language: null,
    object_id: null,
    grades: []
  }
}

export default {
  name: 'VCourses',

  mixins: [pagination],

  components: {
    VPagination,
    IconBase,
    IconCourseList,
    IconCourseGrid,
    IconLessonCount,
    IconLanguage,
    VCourseCard,
    IconSubject
  },

  data() {
    return {
      listView: true,
      courses: [],
      filters: [],
      type: 0,
      showMore: true,
      hasNext: false,
      filterParams: initialFilterParams(),
      currentPage: 0,
      lastPage: 0,
      isAside: false,
    }
  },

  watch: {
    filterParams: {
      async handler(){
        await this.fetchAllCoursesData(true);
      },
      deep: true
    },
    type() {
      Object.assign(this.filterParams, initialFilterParams());
    }
  },

  created() {
    this.fetchAllCoursesData();
    this.fetchFilters();
  },

  methods: {
    async fetchAllCoursesData(isFilter = false) {
      let params = {};
      if (isFilter) {
        Object.assign(params, this.filterParams);
        params.type_id = this.filters.subjectTypes[this.type].id;
      } else {
        this.$store.commit('setLoading', true);
      }
      try {
        params.page = this.currentPage;
        const res = await this.$http({
          url: this.$getAPI('course.allCourses'),
          params: params
        });
        this.courses = res?.rows;
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
        this.hasNext = res?.hasNext;
        this.setDataLayer(this.courses);
      } finally {
        this.$store.commit('setLoading', false);
      }
    },
    async changePage(page) {
      this.currentPage = page;
      await this.fetchAllCoursesData(page)
    },
    async fetchFilters() {
      this.$store.commit('setLoading', true);
      try {
        this.filters = await this.$http({
          url: this.$getAPI('course.courseFilter'),
        });
      } finally {
        this.$store.commit('setLoading', false);
      }
    },
    setDataLayer(courses) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: 'view_item_list',
        ecommerce: {
          items: courses.map((el, index) => ({
            item_name: el.name,
            item_id: el.id,
            price: el.price_string,
            currency: 'Тенге',
            item_brand: "Курсы",
            item_category: "Курсы",
            index: index,
            quantity: index
          }))
        }
      });
    }
  }
}
