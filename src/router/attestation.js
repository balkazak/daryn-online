import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';


export default [
  {
    path: 'attestation',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VAttestation',
        component: () => import('../views/VAttestation').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/:id',
        name: 'VTestAttestation',
        component: () => import('../views/VAttestation/VTestAttestation').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/result/:id',
        name: 'VResultAttestation',
        component: () => import('../views/VAttestation/VResultAttestation').catch(errorHandler),
        meta: {
          authorization: true,
        },
      }
    ]
  }
]
