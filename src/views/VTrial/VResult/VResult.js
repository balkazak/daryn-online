export default {
  name: 'VResultOlympiad',

  data() {
    return {
      percent: '',
      text: '',
    }
  },

  created() {
    this.getFetchResult();
  },

  methods: {
    getFetchResult() {
      const { trialTestId } = this.$route.params;
      this.$store.commit('setLoading', true)
      this.$http({
        url: this.$getAPI('trial.result', {
          trialTestId
        })
      }).then((res) => {
        if (res.errorType === 403) {
          this.$router.push({ name: 'VTrial' });
        } else {
          this.percent = res.results.percent;
          this.text = res.results.text;
        }
      }).finally(() => {
        this.$store.commit('setLoading', false)
      })
    },
  }
}