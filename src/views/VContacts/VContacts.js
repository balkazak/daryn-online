import IconArrowTopRight from "../../components/icons/components/IconArrowTopRight";
import IconFacebookGray from "../../components/icons/components/IconFacebookGray";
import IconInstagramGray from "../../components/icons/components/IconInstagramGray";
import IconTiktokGray from "../../components/icons/components/IconTiktokGray";
import IconYoutubeGray from "../../components/icons/components/IconYoutubeGray";
import IconTelegramGray from "../../components/icons/components/IconTelegramGray";
import IconFacebookWhite from "../../components/icons/components/IconFacebookWhite";
import IconInstagramWhite from "../../components/icons/components/IconInstagramWhite";
import IconTiktokWhite from "../../components/icons/components/IconTiktokWhite";
import IconYoutubeWhite from "../../components/icons/components/IconYoutubeWhite";
import IconTelegramWhite from "../../components/icons/components/IconTelegramWhite";
import IconBase from "../../components/icons/IconBase";

export default {
  name: 'VContacts',

  components: {
    IconArrowTopRight,
    IconFacebookGray,
    IconInstagramGray,
    IconTiktokGray,
    IconYoutubeGray,
    IconTelegramGray,
    IconFacebookWhite,
    IconInstagramWhite,
    IconTiktokWhite,
    IconYoutubeWhite,
    IconTelegramWhite,
    IconBase
  },

  data() {
    return {
      hoverActive: 0,
    }
  },

  methods: {
    makeActive(n) {
      if (n) this.hoverActive = n;
      else this.hoverActive = 0;
    }
  }
}
