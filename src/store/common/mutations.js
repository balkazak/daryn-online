import Vue from 'vue';

export default {
  setLocale(state, locale) {
    window.localStorage.setItem('language', locale);
    window.location.reload();
  },
  setOverlay(state, payload) {
    state.isOverlay = payload;
  },
  setLoading(state, payload) {
    state.loading = payload;
    if (payload) {
      Vue.prototype.$Progress.start();
    } else {
      setTimeout(() => {
        Vue.prototype.$Progress.finish();
      }, 500);
    }
  },
  setVisibleMedia(state, payload) {
    state.visibleMedia = payload;
  },
  setMediaSrc(state, payload) {
    state.mediaSrc = payload;
  },
  setSpinnerLoader(state, payload) {
    state.spinnerLoader = payload;
  },
  setDark(state, payload) {
    state.isDark = payload;
  }
}