import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';

export default [
  {
    path: 'schools',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VSchool',
        component: () => import('../views/VSchool').catch(errorHandler),
        meta: {
          authorization: false,
        },
      },
      {
        path: ':id',
        name: 'VSingleSchool',
        component: () => import('../views/VSchool/VSingleSchool').catch(errorHandler),
        meta: {
          authorization: false,
        },
      }
    ]
  }
]

// import errorHandler from "../helpers/errorHandler";
//
// export default [
//   {
//     path: 'schools',
//     name: 'VSchool',
//     component: () => import('../views/VSchool').catch(errorHandler),
//   },
//   // {
//   //   path: 'courses/:id',
//   //   name: 'VSingleCourse',
//   //   component: () => import('../views/VCourses/VSingleCourse').catch(errorHandler),
//   //   meta: {
//   //     authorization: true,
//   //   },
//   // },
// ]
