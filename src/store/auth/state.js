export default {
  token: window.localStorage.getItem('token') || '',
  user: JSON.parse(window.localStorage.getItem('user')) || '',
  tokenRefreshStatus: ''
}