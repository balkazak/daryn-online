import errorHandler from "../helpers/errorHandler";
import render from "../helpers/renderRouterView";

export default [
  {
    path: 'olympiad',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VOlympiad',
        component: () => import('../views/VOlympiad').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/:olympiadId',
        name: 'VTestOlympiad',
        component: () => import('../views/VOlympiad/VTestOlympiad').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'rating-learner',
        name: 'VRatingLearner',
        component: () => import('../views/VOlympiad/VRatingLearner').catch(errorHandler),
        meta: {
          authorization: true
        }
      },
      {
        path: 'rating',
        name: 'VRatingOlympiad',
        component: () => import('../views/VOlympiad/VRatingOlympiad').catch(errorHandler),
        meta: {
          authorization: true
        }
      },
      {
        path: 'rule-test/:id',
        name: 'VRuleTest',
        component: () => import('../views/VOlympiad/VRuleTest').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/result/:olympiadId',
        name: 'VResultOlympiad',
        component: () => import('../views/VOlympiad/VResult').catch(errorHandler),
        meta: {
          authorization: true,
        },
      }
    ]
  }
]
