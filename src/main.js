import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router';
import store from '@/store';
import Clipboard from 'v-clipboard'
import Bootstrap from 'bootstrap/dist/js/bootstrap';
import BootstrapVue from "bootstrap-vue"
import VueMask from 'v-mask';
import request from "@/services/request";
import API from '@/services/api';
import VNotification from '@/components/VNotification';
import filters from '@/filters'
import VGlModal from '@/components/VGlModal';
import Translator from '@/plugins/Translator';
import Screen from '@/plugins/Screen';
import init from '@/configuration/init';
import VueProgressBar from 'vue-progressbar';
import VueMathjax from 'vue-mathjax';
import VueSmoothScroll from 'vue3-smooth-scroll'
import '@/modals';
import '@/plugins/lightgallery/lightgallery.js';
import '@/plugins/lightgallery/css/lightgallery.css';

Vue.use(VueProgressBar, {
  color: '#007bff',
  failedColor: 'red',
  height: '2px'
});

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(Clipboard);
Vue.use(Bootstrap)
Vue.use(VueMask);
Vue.prototype.$http = request;
Vue.prototype.$getAPI = API;
Vue.use(VNotification);
Vue.use(VGlModal);
Vue.use(Screen);
Vue.use(Translator, {
  store,
});
Vue.use(VueMathjax)
Vue.use(VueSmoothScroll)

filters();

init(store, () => {
  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app')
});
