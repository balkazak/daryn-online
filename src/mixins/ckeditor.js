export default {
    mounted() {
        setTimeout(() => this.initCkeditor(), 2000)
    },

    methods: {
        initCkeditor() {
            const ckeditor = '../../../js/ckeditor/ckeditor.js';
            const config = '../../../js/ckeditor/config.js';
            const configFile = document.createElement('script');
            const ckeditorFile = document.createElement('script');
            configFile.src = config
            ckeditorFile.src = ckeditor
            document.body.appendChild(ckeditorFile)
            document.body.appendChild(configFile)
        },
        removeCkeditor() {
            const ckeditorFile = '/js/ckeditor';
            const scripts = document.querySelectorAll('script');
            scripts.forEach((script) => {
                const { src } = script;
                if (src.indexOf(ckeditorFile) !== -1) {
                    const parent = script.parentNode;
                    parent.removeChild(script);
                }
            });

            const links = document.querySelectorAll('link');
            links.forEach((link) => {
                const { href } = link;
                if (href.indexOf(ckeditorFile) !== -1) {
                    const parent = link.parentNode;
                    parent.removeChild(link);
                }
            })
        }
    },

    beforeDestroy() {
        this.removeCkeditor();
    }
}