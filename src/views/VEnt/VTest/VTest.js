import IconBase from "components/icons/IconBase";
import VRadioGroup from "components/VRadioGroup/VRadioGroup";
import VCheckboxGroup from "components/VCheckboxGroup/VCheckboxGroup";
import {isNumber} from "lodash";

export default {
  name: 'VTest',

  components: {
    IconBase,
    VRadioGroup,
    VCheckboxGroup,
  },

  data() {
    return {
      timer: '',
      isCheck: false,
      objects: [],
      currentObjectIdx: 0,
      currentQuestion: {},
      selectedAnswer: null,
      multipleSelected: [],
      points: 0,
      showCorrect: false,
      entName: ''
    }
  },

  created() {
    this.fetchQuestionsData();
  },

  computed: {
    questions() {
      return this.objects[this.currentObjectIdx]?.questions;
    },
    currentQuestionTitle() {
      return this.currentQuestion.question;
    },
    isMultipleAnswer() {
      return this.currentQuestion.isSeveralAnswer;
    },
    getVariants() {
      return this.currentQuestion.variants || [];
    },
    isAnswered() {
      return this.currentQuestion.isAnswered || false;
    },
    entAnswers() {
      return this.currentQuestion.entAnswers || [];
    },
    userAnswers() {
      return this.currentQuestion.userAnswers || [];
    },
    isLastQuestion() {
      const lastIdx = this.questions?.length - 1;
      const idx = this.questions?.findIndex(el => el.questionId === this.currentQuestion.questionId)
      return idx === lastIdx;
    },
    isFinishQuestion() {
      const objLength = this.objects.length - 1;
      return (this.currentObjectIdx === objLength) && this.isLastQuestion;
    }
  },

  methods: {
    fetchQuestionsData() {
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('ent.getQuestions', {
          id: this.$route.params.entId
        })
      }).then(res => {
        this.setData(res);
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setData(res) {
      this.objects = res.objects;
      this.points = res.points;
      this.entName = this.trans('modules.ent.name').replace(':year', res.year)
      this.setDefaultCurrentQuestion();
      this.togglePagination();
      this.setSelectedAnswer();
      if (res.leftSeconds) {
        this.setTimer(res.leftSeconds);
      } else {
        this.handleFinish();
      }
    },

    setDefaultCurrentQuestion() {
      this.currentQuestion = this.questions?.find(el => el.type === 'info') || this.questions[0];
    },

    setTimer(seconds) {
      let timer;
      timer = setInterval(() => {
        if (seconds >= 0) this.timer = this.getTimer(seconds--);
        else {
          clearInterval(timer);
          this.handleFinish();
        }
      }, 1000);
    },

    getTimer(seconds) {
      if (seconds < 3600) {
        return new Date(seconds * 1000).toISOString().substr(14, 5)
      }
      return new Date(seconds * 1000).toISOString().substr(11, 8)
    },

    getCountQuestions(object) {
      return object.questions?.length;
    },

    handleSelectObject(objectIdx) {
      this.currentObjectIdx = objectIdx;
      this.setDefaultCurrentQuestion();
      this.setSelectedAnswer();
      this.togglePagination();
    },

    handleSelectQuestion(idx) {
      this.currentQuestion = this.questions[idx];
      this.setSelectedAnswer();
      this.togglePagination();
    },

    togglePagination() {
      this.questions.forEach((el) => {
        if (el.type.includes('info')) {
          const idx = el.type.search('info');
          el.type = el.type.slice(0, idx);
        }
        if (el.questionId === this.currentQuestion.questionId) {
          el.type += ' info';
        }
      });
    },

    setSelectedAnswer() {
      if (this.isMultipleAnswer) {
        this.multipleSelected = [...this.currentQuestion.userAnswers];
      } else {
        this.selectedAnswer = this.currentQuestion.userAnswers[0];
      }
    },

    changeMultipleAnswer(event) {
      const answerId = Number(event.target.value);
      if (this.multipleSelected.includes(answerId)) {
        this.multipleSelected = this.multipleSelected.filter(el => el !== answerId);
      } else if (this.multipleSelected.length > 2) {
        event.preventDefault();
      } else {
        this.multipleSelected.push(answerId);
      }
      this.currentQuestion.userAnswers = this.multipleSelected;
    },

    changeAnswer(answerId) {
      this.selectedAnswer = answerId;
      this.currentQuestion.userAnswers = [answerId];
    },

    async sendRequest() {
      const { entId } = this.$route.params;
      const questionId = this.currentQuestion.questionId;
      this.$store.commit('setLoading', true);
      this.$http({
        method: 'POST',
        url: this.$getAPI('ent.sendAnswer', {
          entId,
          questionId
        }),
        data: {
          answers: this.isMultipleAnswer ? this.multipleSelected : [this.selectedAnswer]
        }
      }).then((res) => {
        this.update(res);
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    update(res) {
      this.currentQuestion.type = res.isCorrect ? 'success' : 'danger';
      this.currentQuestion.userAnswers = res.userAnswers;
      this.currentQuestion.entAnswers = res.entAnswers;
      this.currentQuestion.isAnswered = true;
      this.currentQuestion.isCorrect = res?.isCorrect;
      this.points += isNumber(res.point) ? res.point : 0;
    },

    async handleCheck() {
      if (this.multipleSelected.length || this.selectedAnswer) {
        await this.sendRequest();
        this.isCheck = true;
      } else {
        this.$notify({
          type: 'error',
          title: '',
          text: this.trans('general.choose_answer'),
        });
      }
    },

    handleNextQuestion() {
      const index = this.questions.findIndex(el => el.questionId === this.currentQuestion.questionId);
      if (index !== -1) {
        const nextQuestionIdx = index + 1;
        this.handleSelectQuestion(nextQuestionIdx);
      }
    },

    handleFinish() {
      const { entId } = this.$route.params;
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('ent.finishEnt', {
          id: entId
        }),
        method: 'POST'
      }).then(() => {
        this.$router.push({name: 'VResult',
          params: { entId }
        });
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    }
  },

}