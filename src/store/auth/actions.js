import Vue from "vue";
export default {
  auth({ commit }, token) {
    commit('auth', token);
    window.localStorage.setItem('token', token);
  },
  logout({ commit }) {
    commit('logout');
    Vue.prototype.$vGlModal.show({
      modalName: 'v-auth-modal',
      component: 'v-auth-modal',
      modalWidth: 970,
    });
    // if (process.env.NODE_ENV === 'production') {
    //   window.location.href = `${process.env.VUE_APP_DARYN_URL}/auth/logout`;
    //   // window.location.href = `${process.env.VUE_APP_DARYN_URL}/auth/login`;
    // } else {
    //   Vue.prototype.$vGlModal.show({
    //     modalName: 'v-auth-modal',
    //     component: 'v-auth-modal',
    //     modalWidth: 970,
    //   });
    // }
  }
}