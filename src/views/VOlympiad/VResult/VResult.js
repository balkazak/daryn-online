export default {
  name: 'VResultOlympiad',

  data() {
    return {
      percent: '',
      text: '',
      hasDiploma: false,
    }
  },

  created() {
    this.getFetchResult();
  },

  methods: {
    getFetchResult() {
      const { olympiadId } = this.$route.params;
      this.$store.commit('setLoading', true)
      this.$http({
        url: this.$getAPI('olympiad.result', {
          olympiadId
        })
      }).then((res) => {
        if (res.errorType === 403) {
          this.$router.push({ name: 'VOlympiad' });
        } else {
          this.percent = res.results.percent;
          this.text = res.results.total;
          this.hasDiploma = !!res.hasDiploma;
        }
      }).finally(() => {
        this.$store.commit('setLoading', false)
      })
    },

    handleCertificate(type) {
      const url = (type === 'certificate') ? 'olympiad.getCertificateById' : 'olympiad.getDiplomaById'
      const { olympiadId } = this.$route.params;
      this.$http({
        url: this.$getAPI(url, {
          olympiadId
        })
      }).then((res) => {
        if (res.error) {
          this.$notify({
            type: 'error',
            title: '',
            text: res.data.message,
          });
        } else {
          location.href = res.url;
        }
      })
    }
  }
}