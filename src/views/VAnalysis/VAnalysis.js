import VSelect from "@/components/VCustomSelect";
import VSingleSpeciality from "./VSingleSpeciality";
import VProbability from "./VProbability";
import VAnalysisResult from "./VAnalysisResult";
import VPagination from '../../components/VPagination';
import pagination from "@/mixins/pagination";
import VInput from "../../components/VCustomInput";
import IconCheck from "../../components/icons/components/IconCheck";
import IconClose from "../../components/icons/components/IconClose";
import IconBase from "../../components/icons/IconBase";
import Multiselect from 'vue-multiselect'
export default {
    name: 'VAnalysis',
    components: {
        VSelect,
        VSingleSpeciality,
        VPagination,
        VProbability,
        VAnalysisResult,
        VInput,
        IconBase,
        IconCheck,
        IconClose,
        Multiselect
    },
    mixins: [pagination],
    data() {
        return {
            allSpecialities: [],
            filteredSpecialities: [],
            specialityCode: null,
            hasNext: false,
            checkProbability: false,
            resultPage: false,
            firstSubjectList: [],
            secondSubjectList: [],
            articleList: [],
            entScore: null,
            ruralQuota: false,
            secondSubject: '',
            firstSubject: '',
            city: '',
            filterSpeciality: '',
            cityList: [],
            university: '',
            universityList: [],
            currentPage: 0,
            lastPage: 0,
            value: [],
            options: [
                { name: 'Vue.js', language: 'JavaScript' },
                { name: 'Adonis', language: 'JavaScript' },
                { name: 'Rails', language: 'Ruby' },
                { name: 'Sinatra', language: 'Ruby' },
                { name: 'Laravel', language: 'PHP' },
                { name: 'Phoenix', language: 'Elixir' }
            ],
            isHidden: true
        }
    },
    watch: {
        firstSubject: {
            deep: true,
            async handler() {
               await this.fetchSubjects();
            }
        },
        secondSubject: {
            deep: true,
            async handler() {
                  await  this.fetchAllSpecialityData();
            }
        },
        city: {
            deep: true,
            async handler() {
                await  this.getUniversities();
                await  this.fetchAllSpecialityData();
            }
        },
        university: {
            deep: true,
            async handler() {
                await  this.fetchAllSpecialityData();
            }
        }
    },
    mounted() {
        this.fetchAllSpecialityData();
        this.fetchSubjects();
        this.getCities();
        this.fetchSpecialitiesForPlan();
        this.translate();
    },
    methods: {
        translate() {
            this.university.name = this.trans('general.all');
            // this.filterSpeciality.name = this.trans('general.all');
            this.city.name = this.trans('general.all');
            this.firstSubject.name = this.trans('general.choose');
            this.secondSubject.name = this.trans('general.choose');
        },
        isActive(){
            let minEntScore = 0;
            let maxEntScore = 140;
          if (this.firstSubject.value !==null && this.secondSubject.value !==null && this.entScore && this.entScore <= maxEntScore && this.entScore >= minEntScore) return true
        },
        showProbability() {
            this.checkProbability = true
        },
        changeResult() {
          this.resultPage = !this.resultPage
        },
        async fetchAllSpecialityData() {
            this.$store.commit('setLoading', true);
            try {
                const form = this.getForm();
                const res = await this.$http({
                    url: this.$getAPI('analysis.allSpecialities'),
                    params: {
                        page: this.currentPage,
                        ...form
                    }
                });
                this.allSpecialities = res?.rows;
                this.lastPage = res?.lastPage;
                this.currentPage = res?.currentPage;
                this.hasNext = res?.hasNext;
            } catch (error) {
                console.error(error);
            } finally {
                this.$store.commit('setLoading', false);
            }
        },
        async fetchSpecialitiesForPlan() {
            this.$store.commit('setLoading', true);
            try {
                const res = await this.$http({
                    url: this.$getAPI('analysis.allSpecialities'),
                });

                this.specialitiesForPlan = res?.rows;
            } catch (error) {
                console.error(error);
            } finally {
                this.$store.commit('setLoading', false);
            }
        },
        async changePage(page) {
            this.currentPage = page;
            await this.fetchAllSpecialityData(page)
        },
        async fetchSubjects() {
            const form = this.getForm();
            this.secondSubjectList = this.firstSubject.profileSubjects;

            this.$store.commit('setSpinnerLoader', true);
            await this.$http({
                url: this.$getAPI('tutors.subjectDict'),
                params: {
                    page: this.currentPage,
                    ...form
                }
            }).then(res => {
                let subject = res.rows;
                let subjects = [];
                for (let i=0; i<subject.length; i++) {
                    if (subject[i].profileSubjects.length > 0) {
                        subjects.push(subject[i]);
                    }
                }
                this.firstSubjectList = subjects
            }).finally(() => {
                this.$store.commit('setSpinnerLoader', false);
            })
        },
        async getCities() {
            this.$store.commit('setLoading', true);
            await this.$http({
                url: this.$getAPI('dictionary.regions'),
                params: {
                    is_speciality: 1
                }
            }).then(res => {
                this.cityList = res.regions
                this.cityList.unshift({
                    'name': this.trans('modules.speciality.all_cities')})
            }).finally(() => {
                this.$store.commit('setLoading', false);
            })
        },
        async getUniversities() {
            this.university = '';
            this.$store.commit('setLoading', true);
            await this.$http({
                url: this.$getAPI('analysis.universities'),
                params: {
                    region_id: this.city.id
                }
            }).then(res => {
                this.universityList = res.universities;
                this.universityList.unshift({
                    'name': this.trans('general.all')})
            }).finally(() => {
                this.$store.commit('setLoading', false);
            })
        },
        getForm() {
            return {
                object_pair_id: this.secondSubject.objectPairId,
                university_id: this.university.id,
                region_id: this.city.id
            }
        },
        filterSpecialities() {
            let filteredSpecialities = [];
            console.log(this.filterSpeciality)
            for (let i=0; i<this.allSpecialities.length; i++) {
                for (let j=0; j<this.filterSpeciality.length; j++) {
                    if (this.filterSpeciality[j].id === this.allSpecialities[i].id) {
                        filteredSpecialities.push(this.allSpecialities[i])
                    }
                }



                if (this.specialityCode === this.allSpecialities[i].code) {
                    filteredSpecialities.push(this.allSpecialities[i])
                }
            }
            this.filteredSpecialities = filteredSpecialities
        },
        showPlanModal() {
            this.$vGlModal.show({
                modalName: 'v-analysis-plan-modal',
                modalTitle: this.trans('modules.speciality.student_plan'),
                modalWidth: 620,
                component: 'v-analysis-plan-modal',
                componentProps: {
                    specialitiesForPlan: this.specialitiesForPlan,
                }
            });
        },
        clearSubjects(){
            this.firstSubject = '';
            this.secondSubject = '';
            this.entScore = '';
            this.ruralQuota = '';
        },
        showVideoModal() {
            this.$http({
                url: this.$getAPI('dictionary.infoFiles')
            }).then(res => {
                const source = Array.isArray(res) ? res[2]?.source : '';
                this.setMediaSrc(source);
                this.setVisibleMedia(true);
            });
        },
    }
}
