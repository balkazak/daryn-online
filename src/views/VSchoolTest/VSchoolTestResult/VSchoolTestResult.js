export default {
  name: 'VSchoolTestResult',

  data() {
    return {
      percent: '',
      text: '',
    }
  },

  created() {
    this.getFetchResult();
  },

  methods: {
    getFetchResult() {
      const { userTestId } = this.$route.params;
      this.$store.commit('setLoading', true)
      this.$http({
        url: this.$getAPI('schoolTest.result', {
          userTestId
        })
      }).then((res) => {
        if (res.errorType === 403) {
          this.$router.push({ name: 'VSchoolTest' });
        } else {
          this.percent = res.results.percent;
          this.text = res.results.total;
        }
      }).finally(() => {
        this.$store.commit('setLoading', false)
      })
    },
  }
}