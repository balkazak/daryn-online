import errorHandler from "../helpers/errorHandler";

export default [
  {
    path: 'auth/login',
    name: 'VAuth',
    component: () => import('../views/VAuth').catch(errorHandler),
  },
  {
    path: '/email/verify/:userId/:hash',
    name: 'VEmailConfirm',
    component: () => import('../views/VEmailConfirm').catch(errorHandler)
  }
]