import {mapGetters} from "vuex";

export default {
    name: "VBelbinResult",
    components: {

    },
    data() {
        return {

        }
    },
    computed: {
        ...mapGetters([
            'getBelbinResult',
        ])
    },
    methods: {
        showResultModal(item) {
            this.$vGlModal.show({
                modalName: 'v-belbin-result-modal',
                modalTitle: this.trans('modules.belbin.result'),
                modalWidth: 770,
                component: 'v-belbin-result-modal',
                componentProps: {
                    url: item.belbin_role_file,
                    img: item.belbin_role_image,
                    title: item.belbin_role_name
                }
            });
        },

    }
}
