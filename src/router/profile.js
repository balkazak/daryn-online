import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';

export default [
  {
    path: 'profile',
    component: () => import('../views/VProfile').catch(errorHandler),
    meta: {
      authorization: true,
    },
    children: [
      {
        path: '',
        name: 'VProfileMain',
        component: () => import('../views/VProfile/VProfileMain').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'profile'
        },
        // redirect: '/profile/transaction',
      },
      {
        path: 'edit',
        name: 'VProfileEdit',
        component: () => import('../views/VProfile/VProfileEdit').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'profile'
        },
      },
      {
        path: 'article',
        component: {
          render,
        },
        children: [
          {
            path: '',
            name: 'VProfileArticle',
            component: () => import('../views/VProfile/VProfileArticle').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'article'
            },
          },
          {
            path: 'edit/:articleId?',
            name: 'VProfileArticleEdit',
            component: () => import('../views/VProfile/VProfileArticle/VProfileArticleForm')
              .catch(errorHandler),
            meta: {
              authorization: true,
              id: 'article'
            },
          },
          {
            path: 'create',
            name: 'VProfileArticleCreate',
            component: () => import('../views/VProfile/VProfileArticle/VProfileArticleForm').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'article'
            },
          }
        ]
      },
      {
        path: 'favorite',
        name: 'VProfileFavorite',
        component: () => import('../views/VProfile/VProfileFavorite').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'favorite'
        },
      },
      {
        path: 'referral',
        name: 'VProfileReferral',
        component: () => import('../views/VProfile/VProfileReferral').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'referral'
        },
      },
      {
        path: 'transaction',
        name: 'VHistoryTransaction',
        component: () => import('../views/VProfile/VHistoryTransaction').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'transaction'
        },
      },
      {
        path: 'reward',
        name: 'VProfileReward',
        component: () => import('../views/VProfile/VProfileReward').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'reward'
        },
      },
      {
        path: 'tasks',
        component: {
          render
        },
        children: [
          {
            path: '',
            name: 'VProfileTask',
            component: () => import('../views/VProfile/VProfileTask').catch(errorHandler),
            meta: {
              authorization: true,
            },
          },
          {
            path: 'test/:testId',
            name: 'VProfileTaskTest',
            component: () => import('../views/VProfile/VProfileTask/VProfileTaskTest').catch(errorHandler),
            meta: {
              authorization: true,
            },
          },
          // {
          //   path: 'result/:userTestId',
          //   name: 'VSchoolTestResult',
          //   component: () => import('../views/VSchoolTest/VSchoolTestResult').catch(errorHandler),
          //   meta: {
          //     authorization: true,
          //   },
          // },
        ]
      },
      {
        path: 'notification',
        name: 'VProfileNotice',
        component: () => import('../views/VProfile/VProfileNotice').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'notification'
        },
      },
      {
        path: 'history-call',
        component: {
          render,
        },
        children: [
          {
            path: '',
            name: 'VHistoryCall',
            component: () => import('../views/VProfile/VHistoryCall').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'history-call'
            },
          },
          {
            path: 'details',
            name: 'VHistoryCallDetail',
            component: () => import('../views/VProfile/VHistoryCallDetail').catch(errorHandler),
            meta: {
              authorization: true,
              id: 'history-call'
            },
          }
        ]
      },
      {
        path: 'purse',
        name: 'VProfileWallet',
        component: () => import('../views/VProfile/VProfileWallet').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'purse'
        },
      },
      {
        path: 'course',
        name: 'VProfileCourse',
        component: () => import('../views/VProfile/VProfileCourse').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'course'
        },
      },
      {
        path: 'test',
        name: 'VTestResult',
        component: () => import('../views/VProfile/VTestResult').catch(errorHandler),
        meta: {
          authorization: true,
          id: 'test'
        },
      },
      {
        path: 'gradebook',
        name: 'VGradebook',
        component: () => import('../views/VProfile/VGradebook').catch(errorHandler),
        meta: {
          authorization: true
        },
      },
      {
        path: 'schedule',
        name: 'VSchedule',
        component: () => import('../views/VProfile/VSchedule').catch(errorHandler),
        meta: {
          authorization: true
        },
      }
    ]
  }
];
