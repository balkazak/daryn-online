import VRadioGroup from "components/VRadioGroup/VRadioGroup";
import {isNumber} from "lodash";
export default {
  name: 'VTrialTest',

  components: {
    VRadioGroup,
  },

  data() {
    return {
      questions: [],
      timer: '',
      points: 0,
      currentQuestion: {},
      selectedAnswer: null,
      showCorrect: false,
    }
  },

  created() {
    this.fetchQuestionsData();
    // let checkSessionTimeout = function () {
    //   let minutes = Math.abs((initialTime - new Date()) / 1000 / 60);
    //   if (minutes > 20) {
    //     setInterval(function () { location.href = 'Audit.aspx' }, 5000)
    //   }
    // };
    // setInterval(checkSessionTimeout, 1000);
  },

  computed: {
    currentQuestionTitle() {
      return this.currentQuestion.question || '';
    },
    isLastQuestion() {
      const lastIdx = this.questions?.length - 1;
      const idx = this.questions?.findIndex(el => el.questionId === this.currentQuestion.questionId)
      return idx === lastIdx;
    },
    getVariants() {
      return this.currentQuestion.variants;
    },
    isAnswered() {
      return this.currentQuestion.isAnswered;
    },
    userAnswersBlue() {
      return this.currentQuestion.userAnswersBlue || [];
    },
  },

  methods: {
    fetchQuestionsData() {
      const { trialTestId } = this.$route.params
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('trial.getQuestions', { trialTestId })
      }).then(res => {
        if (!res.error) this.setData(res);
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setData(res) {
      if (res.leftSeconds) {
        this.setTimer(res.leftSeconds);
      } else {
        this.handleFinish();
      }
      this.questions = Object.values(res.questions)
      this.questions.forEach(question => { question.status = '' });
      this.setDefaultCurrentQuestion();
      this.togglePagination();
      this.clearAnswer();
    },

    setDefaultCurrentQuestion() {
      this.currentQuestion = this.questions.find(el => el.isAnswered === false) || this.questions[0];
    },

    handleSelectQuestion(idx) {
      if (!this.questions[idx]) {
        this.handleFinish();
      } else {
        this.currentQuestion = this.questions[idx];
        this.togglePagination();
        this.clearAnswer();
      }
    },

    togglePagination() {
      this.questions.forEach((el) => {
        const idx = el.status.search('info');
        el.status = el.status.slice(0, idx);

        if (el.questionId === this.currentQuestion.questionId) el.status = 'info';
      });
    },

    clearAnswer() {
      this.selectedAnswer = null;
    },

    changeAnswer(answerId) {
      this.selectedAnswer = answerId;
      this.currentQuestion.userAnswersBlue = [answerId];
    },

    handleNextQuestion() {
      const index = this.questions.findIndex(el => el.questionId === this.currentQuestion.questionId);
      if (index !== -1) {
        const nextQuestionIdx = index + 1;
        this.handleSelectQuestion(nextQuestionIdx);
      }
    },

    async handleCheck() {
      if (this.selectedAnswer) {
        await this.sendRequest();
      } else {
        this.$notify({
          type: 'error',
          title: '',
          text: this.trans('general.choose_answer'),
        });
      }
    },

    handleFinish() {
      const { trialTestId } = this.$route.params;
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('trial.finish', {
          trialTestId
        }),
        method: 'POST'
      }).then(() => {
        this.$router.push({name: 'VResultTrial', params: { trialTestId }});
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    async sendRequest() {
      const { trialTestId } = this.$route.params;
      const { questionId } = this.currentQuestion;
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        method: 'POST',
        url: this.$getAPI('trial.sendAnswer', {
          trialTestId,
          questionId
        }),
        data: { answer: this.selectedAnswer }
      }).then((res) => {
        if (!res.error) {
          this.currentQuestion.isAnswered = true;
          this.points += isNumber(res.point) ? res.point : 0;
        }
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setTimer(seconds) {
      let timer;
      timer = setInterval(() => {
        if (seconds >= 0) {
          this.timer = this.getTimer(seconds--);
        } else {
          clearInterval(timer);
          this.handleFinish();
        }
      }, 1000);
    },

    getTimer(seconds) {
      if (seconds < 3600) {
        return new Date(seconds * 1000).toISOString().substr(14, 5)
      }
      return new Date(seconds * 1000).toISOString().substr(11, 8)
    },

    getStyle(question) {
      if (question.isAnswered) {
        return {
          cursor: 'default',
          background: '#f5f5f5',
          borderColor: '#f5f5f5',
          color: 'inherit !important'
        }
      }
      return { cursor: 'pointer', }
    },
  },
}