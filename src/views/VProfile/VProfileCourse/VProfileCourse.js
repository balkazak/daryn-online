import IconBase from "@/components/icons/IconBase";
import IconPlay from "@/components/icons/components/IconPlay";
import IconHeart from "@/components/icons/components/IconHeart";
import IconFile from "@/components/icons/components/IconFile";
import VueEasyPieChart from 'vue-easy-pie-chart'
import IconEye from "@/components/icons/components/IconEye";
import 'vue-easy-pie-chart/dist/vue-easy-pie-chart.css'

export default {
  name: "VProfileCourse",

  components: {
    IconBase,
    IconPlay,
    IconHeart,
    IconFile,
    VueEasyPieChart,
    IconEye
  },

  data() {
    return {
      courseList: []
    }
  },

  created() {
    this.fetchCourseData();
  },

  methods: {
    async fetchCourseData() {
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('course.myCourses')
      }).then((res) => {
        this.courseList = res?.subjects;
      }).finally(() => {
        this.$store.commit('setLoading', false);
      })
    },
    handleLike(courseId) {
      this.$http({
        url: this.$getAPI('favorite.deleteOrAddFavorite', {
          subject_id: courseId
        }),
        method: 'POST'
      }).then((res) => {
        if (!res.error) {
          this.$notify({
            type: 'success',
            title: 'Успешно',
            text: 'Успешно!',
          });
        }
      });
    },
  }
}