const state = {
  paymentId: null,
  purchasedCourse: null,
  purchasedOlympiad: null
}

const getters = {
  getPaymentId: (state) => state.paymentId,
  getPurchasedCourse: (state) =>  state.purchasedCourse,
  getPurchasedOlympiad: (state) => state.purchasedOlympiad
}

const mutations = {
  setPaymentId(state, payload) {
    state.paymentId = payload;
  },
  setPurchasedCourse(state, payload) {
    state.purchasedCourse = payload;
  },
  setPurchasedOlympiad(state, payload) {
    state.purchasedOlympiad = payload
  },
}

export default {
  state,
  getters,
  mutations
}
