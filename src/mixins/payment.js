import {mapGetters, mapMutations} from "vuex";

export default {
  beforeDestroy() {
    this.$vGlModal.hide('v-payment-modal');
    this.$vGlModal.hide('v-payment-beeline-modal');
    this.$vGlModal.hide('v-payment-qr-modal');
    this.$vGlModal.hide('v-success-modal');
    this.$vGlModal.hide('v-error-modal');
    clearInterval(this.timer);
  },

  data() {
    return {
      timer: null,
    }
  },

  computed: {
    ...mapGetters([
      'getPurchasedCourse',
      'getPurchasedOlympiad'
    ])
  },

  methods: {
    ...mapMutations([
      'setPurchasedOlympiad'
    ]),
    showPaymentModal(itemId, sum, api, type, lang, olympiad) {
      this.$vGlModal.show({
        modalName: 'v-payment-modal',
        modalTitle: this.trans('payment.pay'),
        modalWidth: 570,
        component: 'v-payment-modal',
        componentProps: {
          sum: sum,
          handleGoToPayment: (object) => {
            const { payment_type } = object;
            if (this.$route.name === 'VOlympiad') {
              this.setEventDataLayer(
                'begin_checkout', olympiad.name, olympiad.id,  'Олимпиада', olympiad.cost
              );
              this.setEventPaymentDataLayer('add_payment_info', payment_type, olympiad);
              this.setPurchasedOlympiad(olympiad)
            }
            if (payment_type === 'beeline') {
              this.showBeelineModal({ itemId, api, type, lang, ...object } );
            } else {
              this.handlePay({ itemId, api, type, lang, ...object  });
            }
          }
        }
      });
    },

    showTeacherPaymentModal() {
      this.$vGlModal.show({
        modalName: 'v-teacher-payment-modal',
        modalTitle: this.trans('payment.pay'),
        modalWidth: 570,
        component: 'v-teacher-payment-modal',
        componentProps: {
          handleGoToPayment: (object) => {
            const { payment_type, cost } = object;
            const api = 'teacherOnline.teacherOnline'
            const type = 'teacherOnline';
            if (payment_type === 'beeline') {
              this.showBeelineModal({api, cost});
            } else {
              this.handlePay({api, cost, type, ...object});
            }
          }
        }
      });
    },

    showBeelineModal(object) {
      this.$vGlModal.show({
        modalName: 'v-payment-beeline-modal',
        modalTitle: this.trans('payment.beeline_title'),
        modalWidth: 770,
        component: 'v-payment-beeline-modal',
        componentProps: {
          handleVerificationCode: (phoneNumber) => this.getVerificationCode({phone_number: phoneNumber, ...object}),
          handleResendCode: () => this.resendCode(),
          handlePayBeeline: (code) => this.payBeeline({ code, ...object }),
        }
      });
    },

    getVerificationCode(object) {
      const { payment_type, use_bonus, itemId, api, phone_number, packet_id = null, subject_id = null } = object;
      this.$store.commit('setSpinnerLoader', true);

      this.$http({
        url: this.$getAPI(api, { id: itemId }),
        method: 'POST',
        data: { payment_type, use_bonus, phone_number, packet_id, subject_id }
      }).then((res) => {
        if (!res?.error) this.$store.commit('setPaymentId', res.paymentId);
        else this.$store.commit('setPaymentId', null);

        this.$store.commit('setSpinnerLoader', false);
      })
    },

    resendCode() {
      this.$http({
        url: this.$getAPI('payment.resend'),
        method: 'POST',
        data: {
          paymentId: this.$store.getters.getPaymentId
        }
      }).then(() => {})
    },

    payBeeline(object) {
      const { code, itemId, type } = object;
      this.$store.commit('setSpinnerLoader', true)
      this.$http({
        url: this.$getAPI('payment.payBeeline'),
        method: 'POST',
        data: {
          code,
          paymentId: this.$store.getters.getPaymentId
        }
      }).then((res) => {
        if (!res.error) {
          this.$vGlModal.hide('v-payment-modal');
          this.$vGlModal.hide('v-payment-beeline-modal');
          this.showSuccessModal(itemId, type);
        }
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false)
      });
    },

    async handlePay(object) {
      const { payment_type, use_bonus, itemId, api, type = '', packet_id = null, subject_id = null, lang = null, cost = null } = object;
      const data = { payment_type, use_bonus }
      if (packet_id) data.packet_id = packet_id;
      if (subject_id) data.subject_id = subject_id;
      if (lang) data.lang = lang;
      if (cost) data.cost = cost;

      const res = await this.$http({
        url: this.$getAPI(api, { id: itemId }),
        method: "POST",
        data: data
      });

      if (!res.error) {
        this.$vGlModal.hide('v-payment-modal');
        this.$vGlModal.hide('v-teacher-payment-modal');

        if (this.$route.name === 'VArticle') this.fetchData();

        switch (payment_type) {
          case 'kaspi':
            if (res.url) {
              this.showQRModal(res);
            } else {
              this.showSuccessModal(itemId, type);
            }
            break;
          case 'wallet-one':
            if (res.form) {
              this.sendSubmit(res.form);
            } else {
              this.showSuccessModal(itemId, type);
            }
            break;
          case 'purse':
            this.showSuccessModal(itemId, type);
            break;
        }
      }
    },

    showQRModal({ url, userRequestId }) {
      const userAgent = navigator.userAgent;
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent) ) {
        location.href = url;
      } else {
        this.$vGlModal.show({
          modalName: 'v-payment-qr-modal',
          modalTitle: this.trans('payment.qr_title'),
          modalWidth: 570,
          component: 'v-payment-qr-modal',
          componentProps: {
            url: url,
            userRequestId: userRequestId,
            callFunction: (userRequestId) => {
              this.timer = setInterval(() => {
                this.loadCheckPayment(userRequestId).then((res) => {
                  if (!res.error && res.isPay) {
                    this.$vGlModal.hide('v-payment-modal');
                    this.$vGlModal.hide('v-payment-qr-modal');
                    if (this.$route.name === 'VSingleCourse' || this.$route.name === 'VCourses') {
                      this.showSuccessModal(this.getPurchasedCourse.id);
                    }
                    if (this.$route.name === 'VOlympiad') {
                      this.showSuccessModal(this.getPurchasedOlympiad.id);
                    }
                    clearInterval(this.timer);
                  }
                })
              }, 3000)
            }
          }
        });
      }
    },

    loadCheckPayment(userRequestId) {
      return new Promise((resolve, reject) => {
        this.$http({
          url: this.$getAPI('payment.check'),
          method: "POST",
          data: { requestId: userRequestId }
        })
          .then((res) => resolve(res))
          .catch((err) => reject(err))
      });
    },

    sendSubmit(form) {
      const div = document.createElement('div');
      document.body.appendChild(div);
      div.innerHTML = form;
      const formData = document.getElementsByTagName('form')[0];
      formData.submit();
    },

    showSuccessModal(id, type) {
      this.$vGlModal.show({
        modalName: 'v-success-modal',
        modalTitle: this.trans('payment.success'),
        modalWidth: 770,
        component: 'v-success-modal',
        componentProps: {
          handleContinue: () => {
            this.$vGlModal.hide('v-success-modal');

            switch (this.$route.name) {
              case 'VOlympiad':
                this.setEventDataLayer(
                  'purchase', this.getPurchasedOlympiad.name, this.getPurchasedOlympiad.id,
                  'Олимпиада', this.getPurchasedOlympiad.cost
                );
                this.$http({ url: this.$getAPI('olympiad.getId', { id }),
                }).then((res) => {
                  if (res?.id) this.$router.push({ name: 'VTestOlympiad', params: { olympiadId: res?.id } });
                });
                break;
              case 'VTrial':
                if (type === 'ent') {
                  this.$http({ url: this.$getAPI('ent.getId', { id }),
                  }).then((res) => {
                    if (res?.id) this.$router.push({ name: 'VEnt', params: { entId: res?.id } });
                  });
                }
                if (type === 'trial') {
                  this.$http({ url: this.$getAPI('trial.getId', { id }),
                  }).then((res) => {
                    if (res?.id) this.$router.push({ name: 'VTrialTest', params: { trialTestId: res?.id } });
                  });
                }
                break;
              case 'VAttestation':
                this.$http({ url: this.$getAPI('attestation.getId', { id }),
                }).then((res) => {
                  if (res?.id) this.$router.push({ name: 'VTestAttestation', params: { id: res?.id } });
                });
                break;
              case 'VSingleCourse':
                this.setEventDataLayer(
                  'purchase', this.getPurchasedCourse.name, this.getPurchasedCourse.id,
                  'Курсы', this.getPurchasedCourse.price_string
                );
                if(type === 'teacherOnline'){
                  //tut budet function
                  location.reload();
                }
                break;
              case 'VCourses':
                this.setEventDataLayer(
                  'purchase', this.getPurchasedCourse.name, this.getPurchasedCourse.id,
                  'Курсы', this.getPurchasedCourse.price_string
                );
                this.$vGlModal.hide('v-booking-modal');
                this.$router.push({ name: 'VProfileCourse' });
                break;
              default:
                this.fetchData();
                this.getCertificateById(id);
                break;
            }
          },
          title: this.trans('payment.success_text'),
          message: this.trans('payment.success_message')
        }
      });
    },

    showErrorModal(object) {
      this.$vGlModal.show({
        modalName: 'v-error-modal',
        modalTitle: 'Упс...',
        modalWidth: 770,
        component: 'v-error-modal',
        componentProps: {
          handleRepeat: () => {
            this.$vGlModal.hide('v-error-modal');
            const { payment_type } = object;
            if (payment_type === 'beeline') {
              this.showBeelineModal(object);
            } else {
              this.handlePay(object);
            }
          }
        }
      });
    },

    getCertificateById(articleId) {
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('article.getCertificateById', { id: articleId }),
      }).then(res => {
        window.location.href = res.url;
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    setEventDataLayer(type, name, id, category, price) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: type,
        ecommerce: {
          value: price,
          currency: "Тенге",
          items: [{
            item_name: name,
            item_id: id,
            price: price,
            currency: 'Тенге',
            item_brand: category,
            item_category: category,
            index: 1,
            quantity: 1
          }]
        }
      });
    },
  }
}
