import IconBase from "@/components/icons/IconBase";
import IconUpgrade from "@/components/icons/components/IconUpgrade";

export default {
  name: 'Verstka',
  components: {
    IconBase,
    IconUpgrade,
  },
}
