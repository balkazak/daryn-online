import request from '@/services/request';
import getAPI from '@/services/api';

async function reqTranslate(store) {
  return request({
    url: getAPI('common.translates'),
    headers: {
      'Accept-Language': store.state.common.lang,
    },
  });
}

export default async function (store, VueInitial) {
  store.commit('setLangLoading', true);

  const res = await reqTranslate(store);
  if (!res.error) {
    store.commit('setLang', res);
  }

  store.commit('setLangLoading', false);

  if (VueInitial) {
    VueInitial();
  }
}
