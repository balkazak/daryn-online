import VSelect from "@/components/VCustomSelect";

export default {
    name: "VProbability",

    data() {
        return {
            firstSpeciality: null,
            secondSpeciality: null,
            thirdSpeciality: null,
            fourthSpeciality: null,
            specialities: {
                firstSpeciality: null,
                secondSpeciality: null,
                thirdSpeciality: null,
                fourthSpeciality: null
            },
            resultPage: false,
        }
    },
    components: {
        VSelect
    },
    props: {
        allSpecialities: Object,
        entScore: Number,
        ruralQuota: Boolean
    },
    methods: {
        async showProbability() {
            this.$emit('changeResult');
            let arrayOfSpecialities = [];
            let arrayOfId = [];
            arrayOfSpecialities = Object.values(this.specialities);
            for (let i=0; i<arrayOfSpecialities.length; i++) {
                if (arrayOfSpecialities[i] != null) arrayOfId.push(arrayOfSpecialities[i].id)
            }
            this.$store.commit('setLoading', true);
            try {
                const res = await this.$http({
                    url: this.$getAPI('analysis.probability'),
                    params: {
                        page: this.currentPage,
                        speciality_array: arrayOfId,
                        point: this.entScore,
                        ruralQuota: this.ruralQuota
                    }
                });
                this.$store.commit('setProbabilityResult', { probabilityResult: res.rows })

            } catch (error) {
                console.error(error);
            } finally {
                this.$store.commit('setLoading', false);
            }
        },
    }
}
