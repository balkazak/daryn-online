export default {
  name: 'VSchedule',

  data() {
    return {
      schedule: [],
    }
  },

  async mounted() {
    await this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setSpinnerLoader', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('schedule.schedule'),
        });
        this.schedule = res.schedule;
      } finally {
        this.$store.commit('setSpinnerLoader', false);
      }
    },
  }
}
