import Vue from 'vue';
import VSuccessModal from "./VSuccessModal";
import VErrorModal from "./VErrorModal";
import VPaymentModal from "./VPaymentModal";
import VPaymentBeelineModal from './VPaymentBeelineModal';
import VPaymentQrModal from './VPaymentQrModal';
import VRequestModal from './VRequestModal';
import VBelbinResultModal from './VBelbinResultModal';
import VAnalysisPlanModal from './VAnalysisPlanModal';
import VAuthModal from './VAuthModal';
import VRestorePasswordModal from './VRestorePasswordModal';
import VBookingModal from './VBookingModal';
import VTeacherPaymentModal from "@/modals/VTeacherPaymentModal";
import VRegistrationModal from '@/modals/VRegistrationModal';
import VInfoRegistrationModal from '@/modals/VInfoRegistrationModal';
import VRegistrationSuccessModal from '@/modals/VRegistrationSuccessModal'

Vue.component('v-success-modal', VSuccessModal)
Vue.component('v-error-modal', VErrorModal)
Vue.component('v-payment-modal', VPaymentModal)
Vue.component('v-teacher-payment-modal', VTeacherPaymentModal)
Vue.component('v-payment-beeline-modal', VPaymentBeelineModal)
Vue.component('v-payment-qr-modal', VPaymentQrModal)
Vue.component('v-request-modal', VRequestModal)
Vue.component('v-belbin-result-modal', VBelbinResultModal)
Vue.component('v-analysis-plan-modal', VAnalysisPlanModal)
Vue.component('v-auth-modal', VAuthModal)
Vue.component('v-restore-password-modal', VRestorePasswordModal)
Vue.component('v-booking-modal', VBookingModal)
Vue.component('v-registration-modal', VRegistrationModal)
Vue.component('v-info-registration-modal', VInfoRegistrationModal)
Vue.component('v-registration-success-modal', VRegistrationSuccessModal)
