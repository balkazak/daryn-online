import { instance } from './axios';
import Vue from 'vue';
import store from '../store';
import getAPI from '../services/api';

export const errorCodes = {
  ERR_401: 401,
  ERR_402: 402,
  ERR_403: 403,
  ERR_404: 404,
  ERR_412: 412,
  ERR_422: 422,
  ERR_423: 423,
  ERR_500: 500,
};

function notify(type, message, title, duration = 5000) {
  const params = {
    type,
    title: title,
    text: message ,
  };

  if (duration) {
    params.duration = duration;
  }

  Vue.prototype.$notify(params);
}

function catchDefault(code, data) {
  const response = {
    error: true,
    errorType: code,
    data: {
      message: data.message ?? data,
    },
  };

  if (!data.action) {
    notify('error', response.data.message);
  }

  return response;
}

async function catch401(data) {
  const response = {
    error: true,
    errorType: errorCodes.ERR_401,
    data: {
      message: data.message ?? data,
    },
  };

  if (!data.action) {
    notify('error', response.data.message);
  }
  await store.dispatch('logout');
}

function catch402(code, data) {
  const response = {
    error: true,
    errorType: code,
    data: {
      message: data.message ?? data,
    },
  };
  return response;
}

// Для ожидания ответа refresh_token
async function waitRefreshToken() {
  return new Promise((res) => {
    const interval = setInterval(() => {
      if (store.state.auth.tokenRefreshStatus !== 'loading') {
        clearInterval(interval);
        res(store.state.auth.tokenRefreshStatus);
      }
    }, 300);
  });
}

async function catch412(data, error) {
  const response = {
    error: true,
    errorType: errorCodes.ERR_412,
    data,
  };
  const originalRequest = error?.config;
  // Отправляем запрос на получение нового токена
  if (store.state.auth.tokenRefreshStatus !== 'loading') {
    store.commit('setTokenRefreshStatus', 'loading');
    const res = await request({
      url: getAPI('auth.refreshToken'),
      method: 'POST'
    }, true);

    if (!res.error) {
      store.dispatch('auth', res.access_token);
      store.commit('setTokenRefreshStatus', 'loaded');
    } else {
      store.commit('setTokenRefreshStatus', 'reject');
      return Promise.reject(res);
    }
  }

  const tokenRefreshStatus = await waitRefreshToken();
  // Если токен успешно обновлён пере отправляем отложенные запросы
  if (tokenRefreshStatus === 'loaded') {
    return request(originalRequest);
  }
  return response;
}

function catch422(data) {
  const response = {
    error: true,
    errorType: errorCodes.ERR_422,
    data,
  };
  if (!data.errors) {
    notify('error', data.message);
  } else if (Array.isArray(data.errors)) {
    notify('error', data.errors[0]);
  } else if (this && data.errors) {
    Object.keys(data.errors).forEach((key) => {
      console.log('key', key)
      console.log('data.errors[key]', data.errors[key])
      notify('error', data.errors[key], '', 5000);
      this.addValidationError(key, data.errors[key]);
    });
  }

  return response;
}

export default async function request(options = {}) {
  try {
    const res = await instance(options);
    return (res && res.data) || res;
  } catch (err) {
    if (err && err.response) {
      const {
        status,
        data,
      } = err.response;

      switch(status) {
        case errorCodes.ERR_401: return catch401.call(this, data);
        case errorCodes.ERR_402: return catch402.call(this, status, data);
        case errorCodes.ERR_412: return catch412.call(this, data, err);
        case errorCodes.ERR_422: return catch422.call(this, data);
        default: return catchDefault.call(this, status, data);
      }
    }

    return err;
  }
}
