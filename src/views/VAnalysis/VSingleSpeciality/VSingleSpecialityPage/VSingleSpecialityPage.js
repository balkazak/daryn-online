import {mapGetters} from "vuex";
import IconMapMarker from "../../../../components/icons/components/IconMapMarker";
import IconBarCode from "../../../../components/icons/components/IconBarCode";
import IconLink from "../../../../components/icons/components/IconLink";
import IconBase from "../../../../components/icons/IconBase";

export default {
    name: "VSingleAnalysis",

    data() {
        return {
            detailSpeciality: null
        }
    },
    components: {
        IconMapMarker,
        IconBase,
        IconBarCode,
        IconLink
    },
    computed: {
        ...mapGetters([
            'getSpecialitySingle',
        ]),
    },

    props: {
        item: Array
    },

    created() {
        this.fetchDetailSpeciality();
    },

    methods: {
        async fetchDetailSpeciality() {
            this.$store.commit('setSpinnerLoader', true);
            try {
                const res = await this.$http({
                    url: this.$getAPI(`analysis.specialityDetail`,
                        {
                            url: this.getSpecialitySingle.url
                        }),
                });
                this.detailSpeciality = res;
            } catch (error) {
                console.error(error);
            } finally {
                this.$store.commit('setSpinnerLoader', false);

            }
        },
    }
}
