import VSubjectCard from "./components/VSubjectCard";
import IconBase from "@/components/icons/IconBase";
import IconLongArrowRight from "@/components/icons/components/IconLongArrowRight";

export default {
  name: 'VEnt',

  components: {
    VSubjectCard,
    IconBase,
    IconLongArrowRight
  },

  data() {
    return {
      mainSubjects: [],
      profileSubjects: [],
      subjects: {},
      langList: []
    }
  },

  created() {
    this.fetchData();
    this.langList = [
      { id: 'kz', title: this.trans('modules.ent.pass_kz') },
      { id: 'ru', title: this.trans('modules.ent.pass_ru') }
    ]
  },

  computed: {
    getCountSelectedSubjects() {
      return (this.mainSubjects.length + this.profileSubjects.length);
    }
  },

  methods: {
    fetchData() {
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('ent.objects'),
        params: {
          user_ent_id: this.$route.params.entId
        }
      }).then((res) => {
        this.subjects = res;
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    handleChange(subject, type) {
      switch (type) {
        case 'profile':
          if (this.profileSubjects.includes(subject.id)) {
            this.profileSubjects = this.profileSubjects.filter(el => el !== subject.id);
          }
          else if (this.profileSubjects.length < 2) this.profileSubjects.push(subject.id);
          break;
        case 'main':
          if (this.mainSubjects.includes(subject.id)) {
            this.mainSubjects = this.mainSubjects.filter(el => el !== subject.id);
          } else this.mainSubjects.push(subject.id);
          break;
      }
    },

    async handleContinue(lang) {
      this.$store.commit('setLoading', true);
      const res = await this.$http({
        url: this.$getAPI('ent.startEnt', {
          id: this.$route.params.entId
        }),
        method: 'POST',
        data: {
          language: lang,
          objects: [...this.profileSubjects, ...this.mainSubjects]
        }
      });
      this.$store.commit('setLoading', false);
      if (!res.error) {
        this.$router.push({ name: 'VTest',
          params: {
            entId: this.$route.params.entId
          }
        });
      }
    },
  }
}