import IconBase from "@/components/icons/IconBase";
import IconDownload from "@/components/icons/components/IconDownload";
import VPagination from '@/components/VPagination';
import pagination from "@/mixins/pagination";

export default {
  name: 'VHistoryCall',

  components: {
    IconBase,
    IconDownload,
    VPagination
  },

  mixins: [pagination],

  data() {
    return {
      calls: []
    }
  },

  async created() {
    await this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('call.historyCall'),
          params: {
            page: this.currentPage,
          }
        });
        this.calls = res.rows;
        this.lastPage = res?.lastPage;
        this.currentPage = res.currentPage;
      } finally {
        this.$store.commit('setLoading', false);
      }
    },

    async changePage(page) {
      this.currentPage = page;
      await this.fetchData()
    }
  }
}