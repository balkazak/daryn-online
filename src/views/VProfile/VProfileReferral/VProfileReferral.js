export default {
  data() {
    return {
      linkText: ''
    }
  },
    async created() {
      await this.fetchReferralLink();
    },
    methods: {
        async fetchReferralLink() {
            try {
                const res = await this.$http({
                    url: this.$getAPI(`course.referralLink`),
                });
                this.linkText = `${process.env.VUE_APP_DARYN_URL}/` + res.referralLink;
            } catch (error) {
                console.error(error);
            }
        },
        showSuccessMessage() {
            this.$notify({
                type: 'success',
                text: 'Скопировано',
                duration: 2000,
            });
        }
    }
}
