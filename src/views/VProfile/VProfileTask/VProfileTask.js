import VPagination from '../../../components/VPagination'
import pagination from "@/mixins/pagination";

export default {
  name: 'VProfileTask',

  mixins: [pagination],

  components: {
    VPagination
  },

  data() {
    return {
      tasks: []
    }
  },

  async mounted() {
    await this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('task.tasks'),
          params: {
            page: this.currentPage,
          }
        });
        this.tasks = res?.testList;
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
      } finally {
        this.$store.commit('setLoading', false);
      }
    },

    async startTest(test) {
      const res = await this.$http({
        url: this.$getAPI('task.getId', { id: test.testTeacherId}),
      });

      if (res.errorType === 404) {
        this.$notify({
          type: 'error',
          title: '',
          text:'Не найдено',
        });
      } else {
        this.$router.push({ name: 'VProfileTaskTest', params: { testId: res.id } })
        // console.log(res.id)
      }
    },

    async changePage(page) {
      this.currentPage = page;
      await this.fetchData()
    }
  }
}