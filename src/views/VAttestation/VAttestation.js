import VSubjectCard from "./components/VSubjectCard";
import IconBase from "@/components/icons/IconBase";
import IconLongArrowRight from "@/components/icons/components/IconLongArrowRight";
import payment from "@/mixins/payment";


export default {
  name: 'VAttestation',
  mixins: [payment],

  components: {
    VSubjectCard,
    IconBase,
    IconLongArrowRight
  },

  data() {
    return {
      selectedSubjects: [],
      subjects: {},
      langList: [
        { id: 'kz', title: 'Сдать на казахском' },
        { id: 'ru', title: 'Сдать на русском' }
      ]
    }
  },

  created() {
    this.fetchData();
  },

  computed: {
    getCountSelectedSubjects() {
      return this.selectedSubjects?.length;
    }
  },

  methods: {
    fetchData() {
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('attestation.getLessons')
      }).then((res) => {
        res.shift();
        this.subjects = res;
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    handleChange(subject) {
      this.selectedSubjects.splice(0,1,subject.id)
    },

    async handleContinue() {
      this.$store.commit('setSpinnerLoader', true);
      const query = this.$route.query
      if (this.selectedSubjects.length !== 0) {
        this.$http({
          url: await this.$getAPI('attestation.getId', {
            id: this.selectedSubjects[0],
          }),
          params: query
        }).then((res) => {
          if (res.errorType === 402) {
            // const api = 'payment.payAttestation';
            // this.showPaymentModal(this.selectedSubjects[0], 1490, api);
          } else {
            this.$router.push({ name: 'VTestAttestation', params: { id: res?.id } });
          }
        }).finally(() => {
          this.$store.commit('setSpinnerLoader', false);
        });
      } else {
        this.$store.commit('setSpinnerLoader', false);
        this.$notify({
          type: 'error',
          title: '',
          text: this.trans('attestation.choose_lesson'),
        });
      }
    },
  }
}
