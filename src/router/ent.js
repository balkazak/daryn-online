import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';
import request from "@/services/request";
import getAPI from '@/services/api';


export default [
  {
    path: 'ent/:entId',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VEnt',
        component: () => import('../views/VEnt').catch(errorHandler),
        meta: {
          authorization: true,
        },
        beforeEnter: (to, from, next) => {
          const { entId } = to.params;
          request({
            url: getAPI('ent.check', {
              id: entId
            })
          }).then((res) => {
            if (res.error) return false;
            else next()
          });
        },
      },
      {
        path: 'test',
        name: 'VTest',
        component: () => import('../views/VEnt/VTest').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/result',
        name: 'VResult',
        component: () => import('../views/VEnt/VResult').catch(errorHandler),
        meta: {
          authorization: true,
        },
      }
    ]
  },
  {
    path: 'test/verstka',
    name: 'Verstka',
    component: () => import('../views/VEnt/Verstka').catch(errorHandler),
    meta: {
      authorization: true,
    },
  }
]
