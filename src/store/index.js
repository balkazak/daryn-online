import Vue from 'vue';
import Vuex from 'vuex';
import common from './common'
import auth from './auth';
import translator from "./translator";
import analysis from "./analysis";
import belbin from "./belbin";
import payment from './payment'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    common,
    translator,
    analysis,
    belbin,
    payment
  }
})
