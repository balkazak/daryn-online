export default {
    name: 'VFaq',
    components: {
    },
    data() {
        return {
            questions: [],
            activeId: 1,
            isAnswered: false
        }
    },
    created() {
        this.fetchQuestionsData();
    },
    methods: {
        fetchQuestionsData() {
            this.$store.commit('setSpinnerLoader', true);
            this.$http({
                url: this.$getAPI('faq.getQuestions')
            }).then(res => {
                this.questions = res.rows;
            }).finally(() => {
                this.$store.commit('setSpinnerLoader', false);
            });
        },
        vote(question, answer) {
            this.$store.commit('setSpinnerLoader', true);
            this.$http({
                url: this.$getAPI('faq.vote'),
                method: 'POST',
                data: {
                    "faq_id": question.faq_id,
                    "vote": answer
                }
            }).then(() => {
                question.is_answered = 1;
            }).finally(() => {
                this.$store.commit('setSpinnerLoader', false);
            });
       }
    }
}
