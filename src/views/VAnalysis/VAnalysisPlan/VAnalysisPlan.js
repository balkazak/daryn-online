import {mapGetters} from "vuex";
import tableAfterNoon from './tables/tableAfterNoon';
import tableBeforeNoon from './tables/tableBeforeNoon';

export default {
    name: "VAnalysisPlan",
    components: {
        tableBeforeNoon,
        tableAfterNoon
    },
    computed: {
        ...mapGetters([
            'getUserPlan'
        ]),
    },
}
