import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';

export default [
  {
    path: 'contacts',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VContacts',
        component: () => import('../views/VContacts').catch(errorHandler),
        meta: {
          authorization: false,
        },
      }
    ]
  }
]
