export default {
  name: 'VResult',

  data() {
    return {
      results: [],
      totalPoint: '',
      percentagePoint: ''
    }
  },

  async created() {
    await this.getFetchResult();
  },

  methods: {
    async getFetchResult() {
      const { entId } = this.$route.params;
      try {
        this.$store.commit('setLoading', true)
        const res = await this.$http({
          url: this.$getAPI('ent.resultEnt', {
            id: entId
          })
        });
        const { results, totalPoint, percentagePoint } = res;
        this.results = results;
        this.totalPoint = totalPoint;
        this.percentagePoint = percentagePoint;
      } finally {
        this.$store.commit('setLoading', false)
      }
    }
  }
}