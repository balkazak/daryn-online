export default {
    name: 'VBelbin',
    components: {
    },
    data() {
        return {
            instructionText: '',
            allQuestions: [],
            selectedStage: 0,
            allAnswers: [
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                },
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                },
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                },
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                },
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                },
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                },
                {
                    'variant1': 0,
                    'variant2': 0,
                    'variant3': 0,
                    'variant4': 0,
                    'variant5': 0,
                    'variant6': 0,
                    'variant7': 0,
                    'variant8': 0,
                }
            ],
            stageActive: 0,
            answerActive: 0,
            rowActive: 0,
            disabled: false,
            value: 0,
            temp_value: 0,
            temp_row: 0,
            sum: 0
        }
    },
    created() {
        this.fetchQuestionsData();
    },
    methods: {
        activeBlue(answer,idx) {
            return this.allAnswers[this.stageActive]['variant'+(idx+1)] > 0
                && this.allAnswers[this.stageActive]['variant'+(idx+1)] >= answer
                || ((this.value >= answer) && this.value != null && this.rowActive === idx)
        },
        fetchQuestionsData() {
            this.$store.commit('setSpinnerLoader', true);
            const query = this.$route.query
            this.$http({
                url: this.$getAPI('belbin.getQuestions'),
                params: query,
            }).then(res => {
                this.allQuestions = res.questions;
                this.instructionText = res['instruction-text'];
            }).finally(() => {
                this.$store.commit('setSpinnerLoader', false);
            });
        },
        giveAnswer(answer, rowIndex) {
            let arrayOfAnswers = this.allAnswers[this.selectedStage]
            let oldValue = arrayOfAnswers['variant'+(rowIndex+1)]

            if(answer === arrayOfAnswers['variant'+(rowIndex+1)]) {
                arrayOfAnswers['variant'+(rowIndex+1)] = 0
                this.sum = this.sum - answer
            }
            else {
                arrayOfAnswers['variant'+(rowIndex+1)] = answer
                this.sum = Object.values(arrayOfAnswers).reduce((a, b) => a + b, 0)
            }

            if (this.sum > 10) {
                arrayOfAnswers['variant'+(rowIndex+1)] = oldValue
                this.sum = Object.values(arrayOfAnswers).reduce((a, b) => a + b, 0)
                this.$notify({
                    type: 'error',
                    title: '',
                    text: this.trans('modules.belbin.error_limit'),
                });
            }
        },
        setStage(index) {
            if (this.sum === 10) {
                this.sum = 0;
                this.value = 0;
                this.rowActive = 0;
                this.answerActive = 0;
                this.selectedStage = index;
                this.stageActive = index;
            }
            else {
                this.$notify({
                    type: 'error',
                    title: '',
                    text: this.trans('modules.belbin.error_sum'),
                });
            }


        },
        showResult() {
            if (this.sum === 10) {
                this.$store.commit('setSpinnerLoader', true);
                this.$http({
                    url: this.$getAPI('belbin.getQuestions'),
                    method: 'POST',
                    data: {
                        answers: this.allAnswers
                    }
                }).then(res => {
                    this.$store.commit('setBelbinResult', { belbinResult: res.test_result })
                    this.$router.push({name: 'VBelbinResult'});
                }).finally(() => {
                    this.$store.commit('setSpinnerLoader', false);
                });
            }
            else {
                this.$notify({
                    type: 'error',
                    title: '',
                    text: this.trans('modules.belbin.error_sum'),
                });
            }
        },
        hoverClass(answer, idx) {
            if (this.disabled==="true") {
                return;
            }
            this.rowActive = idx
            this.temp_value = this.value;
            this.temp_row = this.rowActive;
            this.value = answer;
        },
        hoverOut() {
            if (this.disabled==="true") {
                return;
            }
            this.value = this.temp_value;
            this.rowActive = this.temp_row;
        }
    }
}
