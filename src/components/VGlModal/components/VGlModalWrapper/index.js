export default {
  name: 'VGlModalWrapper',

  data() {
    return {
      items: [],
      zIndex: 999,
    };
  },

  mounted() {
    this.$eventHub.$on('v-gl-modal--show', this.addModal);
    this.$eventHub.$on('v-gl-modal--hide', this.removeModal);
    this.$eventHub.$on('v-gl-modal--hide-all', this.removeModalAll);
  },

  beforeDestroy() {
    this.$eventHub.$off('v-gl-modal--show', this.addModal);
    this.$eventHub.$off('v-gl-modal--hide', this.removeModal);
    this.$eventHub.$off('v-gl-modal--hide-all', this.removeModalAll);
  },

  watch: {
    items(val) {
      if (val.length > 0) {
        window.document.body.classList.add('v-gl-modal__lock-scroll');
      } else {
        window.document.body.classList.remove('v-gl-modal__lock-scroll');
      }
    },
  },

  methods: {
    addModal(obj = {
      modalName: '',
      modalLocked: false,
      modalTitle: '',
      modalClasses: '',
      modalCustom: false,
      modalOverlayDisable: false,
      modalWidth: 0,
      component: '',
      componentProps: {},
    }) {
      this.items.push(obj);
    },

    removeModal(modalName) {
      this.items = this.items.filter((item) => item.modalName !== modalName);
    },

    removeModalAll() {
      this.items = [];
    },

    elModalWindow(obj = {
      modalName: '',
      modalLocked: false,
      modalTitle: '',
      modalClasses: '',
      modalWidth: 0,
      component: '',
      componentProps: {},
    }, idx) {
      return this.$createElement('v-gl-modal', {
        key: obj.modalName,
        props: {
          name: obj.modalName,
          locked: obj.modalLocked,
          title: obj.modalTitle,
          classes: obj.modalClasses,
          width: obj.modalWidth,
          zIndex: this.zIndex + idx + 1,
        },
        scopedSlots: {
          default: ({ params }) => this.elComponent(obj, params),
        },
      });
    },

    elCustomModal(obj = {
      modalName: '',
      modalLocked: false,
      modalTitle: '',
      modalClasses: '',
      component: '',
      componentProps: {},
    }, idx) {
      return this.$createElement(obj.component, {
        key: obj.modalName,
        props: {
          name: obj.modalName,
          locked: obj.modalLocked,
          title: obj.modalTitle,
          classes: obj.modalClasses,
          zIndex: this.zIndex + idx + 1,
          width: obj.modalWidth,
          ...obj.componentProps,
        },
      });
    },

    elComponent(obj = {
      component: '',
      componentProps: {},
    }, params) {
      return this.$createElement(obj.component, {
        props: {
          modalName: obj.modalName,
          ...obj.componentProps,
          ...params,
        },
      });
    },
  },

  render(h) {
    return h('div', {
      staticClass: 'v-gl-modal-wrapper',
    }, [
      (this.items.length > 0 && !this.items[this.items.length - 1].modalOverlayDisable)
      && h('div', {
        staticClass: 'v-gl-modal__overlay',
        style: `z-index: ${this.zIndex + this.items.length}`,
      }),
      h('transition-group', {
        props: {
          name: 'ol-transition--fade',
          tag: 'div',
        },
      }, this.items.map((item, itemIdx) => {
        if (item.modalCustom) {
          return this.elCustomModal(item, itemIdx);
        }

        return this.elModalWindow(item, itemIdx);
      })),
    ]);
  },
};
