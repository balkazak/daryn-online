import IconBase from "../../../components/icons/IconBase";
import IconArrowRight from "../../../components/icons/components/IconArrowRight";

export default {
    name: "VSingleSchool",

    data() {
        return {
            school: null
        }
    },

    components: {
        IconBase,
        IconArrowRight,
    },

    mounted() {
        this.getSchoolInfo();
    },

    methods: {
        async getSchoolInfo(){
            this.$store.commit('setSpinnerLoader', true);
            //убираем начальное слово в линке до знак "/"
            let url = this.$route.params?.id.slice(1)
            url = url.substring(url.indexOf("/") + 1);
            try {
                const res = await this.$http({
                    url: this.$getAPI('schools.getSingleSchool',
                        {
                            id: url
                        }),
                });
                this.school = res;
            } catch (error) {
                console.error(error);
            } finally {
                this.$store.commit('setSpinnerLoader', false);
            }
        },
        gotoTrial () {
            let routeData = this.$router.resolve({name: 'VTrial'});
            window.open(routeData.href, '_blank');
        },
        gotoNavigator () {
            let url = "https://edunavigator.kz/ru?utm_source=darynonline&utm_medium=banner&utm_campaign=darynonlinespring"
            window.open(url, '_blank');
        },
        getClasses(item) {
            switch (item.competition) {
                case 'Орташа':
                case 'Средняя':
                    return 'warning'
                case 'Жоғары':
                case 'Высокая':
                    return 'danger'
                default:
                    return 'success'
            }
        }
    }
}
