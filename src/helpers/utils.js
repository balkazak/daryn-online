const jsonToQueryString = (json) => {
  return json ? ('?' +
    Object.keys(json).filter((key) => {return json[key] && json[key] !== 'null'}).map(function(key) {
      return encodeURIComponent(key) + '=' +
        encodeURIComponent(json[key]);
    }).join('&')) : '';
}

export {
  jsonToQueryString
}