export default [
    {
        title: 'modules.search.search_category.course',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M14.1667 15H5.83333C5.37333 15 5 14.6267 5 14.1667V10C5 9.54001 5.37333 9.16667 5.83333 9.16667H14.1667C14.6267 9.16667 15 9.54001 15 10V14.1667C15 14.6267 14.6267 15 14.1667 15Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M4.16667 17.5H15.8333C16.7542 17.5 17.5 16.7542 17.5 15.8333V4.16667C17.5 3.24583 16.7542 2.5 15.8333 2.5H4.16667C3.24583 2.5 2.5 3.24583 2.5 4.16667V15.8333C2.5 16.7542 3.24583 17.5 4.16667 17.5Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M17.5 6.66667H2.5" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'subject'
    },
    {
        title: 'modules.search.search_category.page',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M9.53667 18.3333H4.25917C3.2875 18.3333 2.5 17.5458 2.5 16.5742V7.7775C2.5 6.80583 3.2875 6.01833 4.25917 6.01833H9.53667C10.5083 6.01833 11.2958 6.80583 11.2958 7.7775V16.5742C11.2967 17.5458 10.5083 18.3333 9.53667 18.3333Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M8.12666 6.01834L8.9425 2.97084C9.19416 2.0325 10.1592 1.475 11.0975 1.72667L16.195 3.0925C17.1333 3.34417 17.6908 4.30834 17.4392 5.2475L15.1625 13.7442C14.9108 14.6825 13.9467 15.24 13.0083 14.9883L11.2958 14.53" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'page'
    },
    {
        title: 'modules.search.search_category.test',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M4.15221 6.35265H7.59114" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M2.49685 10C2.49101 12.1368 3.399 14.1743 4.99195 15.5986C6.5849 17.0229 8.7108 17.6982 10.8337 17.4542" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M9.16632 2.54577C11.2892 2.30182 13.4151 2.97712 15.0081 4.40143C16.601 5.82574 17.509 7.86316 17.5032 10" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M5.99188 2.91371L3.53897 7.49895" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M8.12421 7.49895L5.99188 2.91371" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M13.3347 16.6694V11.6674" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M13.3347 16.6694H15.8558C16.5354 16.6694 17.0863 16.1185 17.0863 15.4389V15.4389C17.0863 14.7593 16.5354 14.2084 15.8558 14.2084H13.3347" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M13.3347 11.6674H15.5445C16.2462 11.6674 16.8151 12.2362 16.8151 12.9379V12.9379C16.8151 13.6396 16.2462 14.2084 15.5445 14.2084H13.3347" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'special-test'
    },
    {
        title: 'modules.search.search_category.article',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M7.49896 6.68002H12.501" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M7.49896 10H10" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M3.74739 3.33055H13.3347C14.2556 3.33055 15.0021 4.07705 15.0021 4.99791V13.3347" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path d="M6.24843 16.6694H15.8358C16.7566 16.6694 17.5031 15.9229 17.5031 15.0021V14.1684C17.5031 13.708 17.1299 13.3347 16.6694 13.3347H8.33264C7.87221 13.3347 7.49896 13.708 7.49896 14.1684V15.4189C7.49896 16.1096 6.93908 16.6694 6.24843 16.6694V16.6694C5.55779 16.6694 4.99791 16.1096 4.99791 15.4189V4.58107C4.99791 3.89043 4.43804 3.33055 3.74739 3.33055V3.33055C3.05675 3.33055 2.49687 3.89043 2.49687 4.58107V6.66527C2.49687 7.1257 2.87012 7.49895 3.33055 7.49895H4.99791" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'article'
    },
    {
        title: 'modules.search.search_category.questions',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M11.6669 8.74905L9.58483 10.8337L8.3343 9.58315" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M10 3.33054C5.8566 3.33054 2.49687 6.1306 2.49687 9.58072C2.5595 11.6627 3.70771 13.5598 5.52313 14.5809C5.3428 15.0709 5.09205 15.5321 4.77877 15.9499C4.60586 16.2023 4.61014 16.5362 4.78946 16.7841C4.96878 17.032 5.2845 17.1406 5.57841 17.0555C6.58076 16.7637 7.52423 16.2984 8.36598 15.6809C8.90476 15.7819 9.45183 15.8321 10 15.8309C14.1434 15.8309 17.5031 13.0308 17.5031 9.58072C17.5031 6.13061 14.1434 3.33054 10 3.33054Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'faq'
    },
    {
        title: 'modules.search.search_category.news',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M4.16667 11.6666V11.6666C3.24583 11.6666 2.5 10.9208 2.5 9.99998V9.99998C2.5 9.07915 3.24583 8.33331 4.16667 8.33331V8.33331C5.0875 8.33331 5.83333 9.07915 5.83333 9.99998V9.99998C5.83333 10.9208 5.0875 11.6666 4.16667 11.6666Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M9.99999 11.6666V11.6666C9.07916 11.6666 8.33333 10.9208 8.33333 9.99998V9.99998C8.33333 9.07915 9.07916 8.33331 9.99999 8.33331V8.33331C10.9208 8.33331 11.6667 9.07915 11.6667 9.99998V9.99998C11.6667 10.9208 10.9208 11.6666 9.99999 11.6666Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M15.8333 11.6666V11.6666C14.9125 11.6666 14.1667 10.9208 14.1667 9.99998V9.99998C14.1667 9.07915 14.9125 8.33331 15.8333 8.33331V8.33331C16.7542 8.33331 17.5 9.07915 17.5 9.99998V9.99998C17.5 10.9208 16.7542 11.6666 15.8333 11.6666Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M4.16667 17.5V17.5C3.24583 17.5 2.5 16.7542 2.5 15.8334V15.8334C2.5 14.9125 3.24583 14.1667 4.16667 14.1667V14.1667C5.0875 14.1667 5.83333 14.9125 5.83333 15.8334V15.8334C5.83333 16.7542 5.0875 17.5 4.16667 17.5Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M9.99999 17.5V17.5C9.07916 17.5 8.33333 16.7542 8.33333 15.8334V15.8334C8.33333 14.9125 9.07916 14.1667 9.99999 14.1667V14.1667C10.9208 14.1667 11.6667 14.9125 11.6667 15.8334V15.8334C11.6667 16.7542 10.9208 17.5 9.99999 17.5Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M15.8333 17.5V17.5C14.9125 17.5 14.1667 16.7542 14.1667 15.8334V15.8334C14.1667 14.9125 14.9125 14.1667 15.8333 14.1667V14.1667C16.7542 14.1667 17.5 14.9125 17.5 15.8334V15.8334C17.5 16.7542 16.7542 17.5 15.8333 17.5Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M4.16667 5.83333V5.83333C3.24583 5.83333 2.5 5.0875 2.5 4.16667V4.16667C2.5 3.24583 3.24583 2.5 4.16667 2.5V2.5C5.0875 2.5 5.83333 3.24583 5.83333 4.16667V4.16667C5.83333 5.0875 5.0875 5.83333 4.16667 5.83333Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M9.99999 5.83333V5.83333C9.07916 5.83333 8.33333 5.0875 8.33333 4.16667V4.16667C8.33333 3.24583 9.07916 2.5 9.99999 2.5V2.5C10.9208 2.5 11.6667 3.24583 11.6667 4.16667V4.16667C11.6667 5.0875 10.9208 5.83333 9.99999 5.83333Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M15.8333 5.83333V5.83333C14.9125 5.83333 14.1667 5.0875 14.1667 4.16667V4.16667C14.1667 3.24583 14.9125 2.5 15.8333 2.5V2.5C16.7542 2.5 17.5 3.24583 17.5 4.16667V4.16667C17.5 5.0875 16.7542 5.83333 15.8333 5.83333Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'news'
    },
    {
        title: 'modules.search.search_category.olympiad',
        img: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M12.5 14.1667H15.8333C16.7538 14.1667 17.5 13.4205 17.5 12.5V6.21176C17.5 5.54872 17.2366 4.91283 16.7678 4.44399L15.556 3.23223C15.0872 2.76339 14.4513 2.5 13.7882 2.5H9.13084C8.21036 2.5 7.46417 3.24619 7.46417 4.16667V5.83333" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '<path fill-rule="evenodd" clip-rule="evenodd" d="M11.7678 7.77734L10.556 6.56558C10.0872 6.09674 9.45128 5.83334 8.78824 5.83334H4.16667C3.24619 5.83334 2.5 6.57954 2.5 7.50001V15.8333C2.5 16.7538 3.24619 17.5 4.16667 17.5H10.8333C11.7538 17.5 12.5 16.7538 12.5 15.8333V9.5451C12.5 8.88206 12.2366 8.24618 11.7678 7.77734Z" stroke="#6D7885" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '</svg>',
        id: 'olympiad'
    },
]