const teacherClient = `https://teacher.daryn.online/call_teacher?rand=${Math.random()}`;
const apiKey = "12345";

export default {
  mounted() {

    let lastUrl = location.href; 
    new MutationObserver(() => {
      const url = location.href;
      if (url !== lastUrl) {
        lastUrl = url;
        onUrlChange();
      }
    }).observe(document, {subtree: true, childList: true});

    function onUrlChange() {
      // alert('URL changed!', location.href);
      if (/^.*\/courses\/\d+.*$/.test(document.location)) {
        return; // still in the same page
      }
      const teacherIframe = document.getElementById('teacherIframe');
      teacherIframe.parentNode.removeChild(teacherIframe);
      const teacherIframeDiv = document.getElementById('teacherIframeDiv');
      teacherIframeDiv.parentNode.removeChild(teacherIframeDiv);
    }


    //TODO somehow set all these data
    const topUpBalanceCallback = () => {
      // TODO: modify this method
      this.showTeacherPaymentModal()
    }

    var div = document.createElement("div");
    div.setAttribute("id", "teacherIframeDiv");
    div.style.cssText = "position: fixed; width:350px; height:220px; bottom: 0; right: 0; z-index:2000";
    document.body.appendChild(div);

    const fetchCourses = () => {
      this.$http({
        url: this.$getAPI('course.getCourseById', {
          id: this.$route.params.id
        })
      }).then((res) => {
        const user = JSON.parse(window.localStorage.getItem('user'))
        const userId = user ? user.user_id : null;
        const name = user ? user.name : null;
        const sectionId = res.teacherSectionId;
        const degreeId = res.teacherDegreeId;
        const languageId = res.teacherLanguageId;

        if(sectionId) {
          document.body.classList.add("is-teacher-frame");

          var iframe = document.createElement("iframe");
          iframe.setAttribute("id", "teacherIframe");
          iframe.style.cssText = "border:none; overflow:hidden; width:100%; height:100%; position: absolute; bottom:0;";
          iframe.setAttribute("src", teacherClient);
          document.getElementById("teacherIframeDiv").appendChild(iframe);

          window.addEventListener('message', function(event) {
            // var origin = event.origin || event.originalEvent.origin; // check the origin
            if (typeof event.data == 'object' && event.data.call==="callback") {
              if (event.data.value === 'ready') {
                document.getElementById("teacherIframe").contentWindow.postMessage({
                  call:'sendCallMetaData',
                  value: {
                    "api_key": apiKey,
                    "user_id": userId,
                    "name": name,
                    "sectionId": sectionId,
                    "degreeId": degreeId,
                    "languageId": languageId,
                    "topUpBalanceCallback": encodeURI(topUpBalanceCallback.toString())
                  }
                }, "*");
              }
            }
          }, false);
        } else {
          document.getElementById("teacherIframeDiv").style.display = "none";
        }
      })
    }

    fetchCourses()

    window.addEventListener('message', function(event) {
      // var origin = event.origin || event.originalEvent.origin; // check the origin
      if (typeof event.data == 'object' && event.data.call==="callback") {
        if (event.data.value === 'topUpBalance') topUpBalanceCallback();
      }
    }, false);
  }
}

