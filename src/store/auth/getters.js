export default {
  isLoggedIn: (state) => !!state.token,
  profileAvatar: (state) => state.user?.avatar,
  getUserFullName: (state) => state.user?.name,
  getDarynSum: (state) => state.user?.daryn,
  getUserId: (state) => state.user?.user_id,
  getMoney: (state) => state.user?.money
}