import VRadioGroup from "components/VRadioGroup/VRadioGroup";
import VCheckboxGroup from "components/VCheckboxGroup/VCheckboxGroup";
import draggable from 'vuedraggable'
import {isNumber} from "lodash";
export default {
  name: 'VTestOlympiad',

  components: {
    VRadioGroup,
    VCheckboxGroup,
    draggable
  },

  data() {
    return {
      questions: [],
      timer: '',
      points: 0,
      currentQuestion: {},
      selectedAnswer: null,
      multipleSelected: [],
      showCorrect: false,
      answerText: '',
      dragSrcEl: null,
      currentlyDragging: null,
      loggedEvent: '',
      rightQuestions: [],
      isToggleMatch: false
    }
  },

  created() {
    this.fetchQuestionsData();
    // let checkSessionTimeout = function () {
    //   let minutes = Math.abs((initialTime - new Date()) / 1000 / 60);
    //   if (minutes > 20) {
    //     setInterval(function () { location.href = 'Audit.aspx' }, 5000)
    //   }
    // };
    // setInterval(checkSessionTimeout, 1000);
    this.getPaymentInfo();
  },

  computed: {
    currentQuestionTitle() {
      return this.currentQuestion.question || '';
    },
    audioLink() {
      return this.currentQuestion.audioLink;
    },
    isLastQuestion() {
      const lastIdx = this.questions?.length - 1;
      const idx = this.questions?.findIndex(el => el.questionId === this.currentQuestion.questionId)
      return idx === lastIdx;
    },
    getVariants() {
      return this.currentQuestion.variants;
    },
    isAnswered() {
      return this.currentQuestion.isAnswered;
    },
    isMultipleAnswer() {
      return this.currentQuestion.type === 'several_answer';
    },
    isSingleAnswer() {
      return this.currentQuestion.type === 'single_answer';
    },
    isOpenQuestion() {
      return this.currentQuestion.type === 'open_question';
    },
    isMatch() {
      return this.currentQuestion.type === 'match' || false;
    },
    matchLeftQuestion() {
      return this.currentQuestion.leftSide;
    },
    matchRightQuestion() {
      return this.currentQuestion.rightSide;
    }
  },

  methods: {
    fetchQuestionsData() {
      const { olympiadId } = this.$route.params;
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('olympiad.getQuestions', { olympiadId })
      }).then(res => {
        if (!res.error) {
          this.setData(res);
        }
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setData(res) {
      if (res.leftSeconds !== 0) {
        this.setTimer(res.leftSeconds);
      } else {
        this.handleFinish();
      }
      if (!res.questions) return;
      this.points = res.points;
      this.questions = res.questions.map((el) => {
        return {
          ...el,
          status: el.status
        }
      });
      this.setDefaultCurrentQuestion();
      this.togglePagination();
      this.clearAnswer();
      setTimeout(() => this.isToggleMatch = true, 300)
      setTimeout(() => this.setHeight(), 1000)
    },

    setDefaultCurrentQuestion() {
      this.currentQuestion = this.questions.find(el => el.status === 'info') || this.questions[0];
    },

    handleSelectQuestion(idx) {
      if (!this.questions[idx]) {
        this.handleFinish();
      } else {
        this.currentQuestion = this.questions[idx];
        this.togglePagination();
        this.clearAnswer();
        setTimeout(() => this.isToggleMatch = true, 300)
        setTimeout(() => this.setHeight(), 1000)
      }
    },

    togglePagination() {
      this.questions.forEach((el) => {
        if (el.status.includes('info')) {
          const idx = el.status.search('info');
          el.status = el.status.slice(0, idx);
        }
        if (el.questionId === this.currentQuestion.questionId) {
          el.status += ' info';
        }
      });
    },

    clearAnswer() {
      this.multipleSelected = [];
      this.selectedAnswer = null;
      this.answerText = '';
      this.rightQuestions = [];
      this.isToggleMatch = false;
    },

    changeMultipleAnswer(event) {
      const answerId = Number(event.target.value);
      if (this.multipleSelected.includes(answerId)) {
        this.multipleSelected = this.multipleSelected.filter(el => el !== answerId);
      } else {
        this.multipleSelected.push(answerId);
      }
      this.currentQuestion.userAnswers = this.multipleSelected;
    },

    changeAnswer(answerId) {
      this.selectedAnswer = answerId;
      this.currentQuestion.userAnswers = [answerId];
    },

    handleNextQuestion() {
      const index = this.questions.findIndex(el => el.questionId === this.currentQuestion.questionId);
      if (index !== -1) {
        const nextQuestionIdx = index + 1;
        this.handleSelectQuestion(nextQuestionIdx);
      }
    },

    async handleCheck() {
      if (this.selectedAnswer || this.multipleSelected?.length || this.answerText || this.matchRightQuestion?.length) {
        await this.sendRequest();
      } else {
        this.$notify({
          type: 'error',
          title: '',
          text: this.trans('general.choose_answer'),
        });
      }
    },

    handleFinish() {
      const { olympiadId } = this.$route.params;
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('olympiad.finish', { olympiadId }),
        method: 'POST'
      }).then(() => {
        this.$router.push({name: 'VResultOlympiad', params: { olympiadId }});
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    getParams() {
      if (this.isOpenQuestion) {
        return {
          type: 'open_question',
          open_question_answer: this.answerText
        }
      } else if (this.isMatch) {
        const inputs = document.querySelectorAll('.compare-input');
        this.rightQuestions = Array.from(inputs).map(el => el.value);
        return {
          type: 'match',
          match_answers: [
            this.matchLeftQuestion.map(el => el.id),
            this.rightQuestions
          ]
        }
      } else {
        return {
          type: 'test',
          test_answers: this.isMultipleAnswer ? this.multipleSelected : [this.selectedAnswer],
        }
      }
    },

    async sendRequest() {
      const { olympiadId } = this.$route.params;
      const questionId = this.currentQuestion.questionId;
      let params = this.getParams();
      this.$store.commit('setLoading', true);
      this.$http({
        method: 'POST',
        url: this.$getAPI('olympiad.sendAnswer', {
          olympiadId,
          questionId
        }),
        data: params
      }).then((res) => {
        if (!res.error) {
          this.currentQuestion.isAnswered = true;
          this.points += isNumber(res.point) ? res.point : 0;
          this.handleNextQuestion();
        }
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    setTimer(seconds) {
      let timer;
      timer = setInterval(() => {
        if (seconds >= 0) {
          this.timer = this.getTimer(seconds--);
        } else {
          clearInterval(timer);
          this.handleFinish();
        }
      }, 1000);
    },

    getTimer(seconds) {
      if (seconds < 3600) {
        return new Date(seconds * 1000).toISOString().substr(14, 5)
      }
      return new Date(seconds * 1000).toISOString().substr(11, 8)
    },

    getStyle(question) {
      if (question.isAnswered) {
        return {
          cursor: 'default',
          background: '#f5f5f5',
          borderColor: '#f5f5f5'
        }
      }
      return { cursor: 'default'}
    },

    changeText(event) {
      event.target.style.height = 'auto';
      event.target.style.height = event.target.scrollHeight + 'px';
    },

    setHeight() {
      let tags = [];
      tags = document.getElementsByClassName('compare-test-item');
      let arr = Array.from(tags).map(tag => tag.clientHeight);
      let height = Math.max(...arr);
      Array.from(tags).forEach((el) => {
        el.style.height = height + 'px'
      });
    },

    handleDragStart(e) {
      e.target.style.opacity = '0.4';
      this.dragSrcEl = e.srcElement;
      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setData('text/html', e.target.innerHTML);
    },
    handleDragEnd(e) {
      e.target.style.opacity = '1';
      let items = document.querySelectorAll('.compare-answer');
      items.forEach(function (item) {
        item.classList.remove('over');
      });
    },
    handleDragOver(e) {
      if (e.preventDefault) {
        e.preventDefault();
      }
      return false;
    },
    handleDragEnter(e) {
      e.target.classList.add('over');
    },
    handleDragLeave(e) {
      e.target.classList.remove('over');
    },
    handleDrop(e) {
      e.stopPropagation();

      if (this.dragSrcEl !== e.srcElement) {
        this.dragSrcEl.innerHTML = e.target.innerHTML;
        e.target.innerHTML = e.dataTransfer.getData('text/html');
      }

      return false;
    },

    getPaymentInfo() {
      const { olympiadId } = this.$route.params;
      this.$http({
        url: this.$getAPI('payment.getPaymentInfo'),
        params: {
          entity_kind_id: 2,
          entity_id: olympiadId
        }
      }).then((res) => {
        if (!res.error) {
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push(res)
        }
      })
    }
  },
}