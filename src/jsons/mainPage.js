
export const tabs = [
  {
    title: 'modules.homepage.main_tab.school',
    id: 'student',
    img: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<path d="M6 18.934H5C3.895 18.934 3 18.039 3 16.934V14.481C3 13.328 3.935 12.393 5.088 12.393H6" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M18 18.934H19C20.105 18.934 21 18.039 21 16.934V14.434C21 13.329 20.105 12.434 19 12.434H18" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M14 17.894H10C9.448 17.894 9 17.446 9 16.894V14.894C9 14.342 9.448 13.894 10 13.894H14C14.552 13.894 15 14.342 15 14.894V16.894C15 17.447 14.552 17.894 14 17.894Z" stroke="#000000" stroke-width="1.6432" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M15 6V5C15 3.895 14.105 3 13 3H11C9.895 3 9 3.895 9 5V6" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M13 10H11" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M14 6H10C7.791 6 6 7.791 6 10V19C6 20.105 6.895 21 8 21H16C17.105 21 18 20.105 18 19V10C18 7.791 16.209 6 14 6Z" stroke="#000000" stroke-width="1.5492" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '</svg>'
  },
  {
    title: 'modules.homepage.main_tab.teacher',
    id: 'teacher',
    img: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<path d="M12.0002 7.99826V4.86095" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M19.5595 21.0037H14.9597C14.2907 21.0037 13.666 20.6694 13.2949 20.1127L11.2613 17.0621C10.8037 16.3757 10.9617 15.4514 11.6213 14.9559C12.2835 14.5004 13.1787 14.5944 13.7317 15.1777L14.5557 16.0017V10.4994C14.5557 9.6706 15.2276 8.99875 16.0564 8.99875C16.8851 8.99875 17.557 9.6706 17.557 10.4994V13.0223L20.3257 13.4739C21.3876 13.6471 22.1224 14.6291 21.989 15.6967L21.5448 19.251C21.4197 20.2523 20.5686 21.0037 19.5595 21.0037Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M7.99832 15.1742C6.79083 15.0661 5.57777 15.3088 4.50487 15.8733C4.19548 16.0519 3.81456 16.053 3.50414 15.8762C3.19371 15.6993 3.00039 15.3711 2.99617 15.0139V5.79134C2.99072 5.15276 3.2931 4.55061 3.80858 4.17366C6.34101 2.37347 9.80294 2.66393 12 4.86095C14.197 2.66393 17.659 2.37347 20.1914 4.17366C20.7069 4.55061 21.0093 5.15276 21.0038 5.79134V9.99909" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '</svg>'
  },
  {
    title: 'modules.homepage.main_tab.parent',
    id: 'parent',
    img: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M17.2261 15.1575L17 15.3871L16.7738 15.1575C16.3624 14.737 15.7989 14.5 15.2107 14.5C14.6224 14.5 14.0589 14.737 13.6475 15.1575V15.1575C12.7842 16.0395 12.7842 17.4499 13.6475 18.332L15.7626 20.4796C16.0883 20.8124 16.5343 21 17 21C17.4657 21 17.9117 20.8124 18.2374 20.4796L20.3525 18.332C21.2158 17.4499 21.2158 16.0396 20.3525 15.1575V15.1575C19.941 14.737 19.3776 14.5 18.7893 14.5C18.201 14.5 17.6375 14.737 17.2261 15.1575Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M10 15H7C4.79086 15 3 16.7909 3 19V20" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<circle cx="11" cy="7.00001" r="4" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '</svg>'
  },
  {
    title: 'modules.homepage.main_tab.university',
    id: 'school',
    img: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<path d="M10.499 21.0037V17.5023C10.499 16.6735 11.1709 16.0017 11.9996 16.0017V16.0017C12.8284 16.0017 13.5003 16.6735 13.5003 17.5023V21.0037" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M6.99805 21.0037V11.4998C6.99805 10.8172 7.34623 10.1817 7.92143 9.81408L10.9227 7.89629C11.5796 7.47645 12.4206 7.47645 13.0776 7.89629L16.0788 9.81408C16.654 10.1817 17.0022 10.8172 17.0022 11.4998V21.0037" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M12 2.99625H16.0017V5.49729H12" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M12.0002 7.58116V2.99625" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M12.0002 11.3327C11.908 11.3333 11.8337 11.4083 11.8341 11.5005C11.8345 11.5926 11.9094 11.667 12.0015 11.6669C12.0937 11.6667 12.1683 11.5919 12.1683 11.4998C12.1685 11.4552 12.1509 11.4124 12.1193 11.381C12.0877 11.3496 12.0447 11.3322 12.0002 11.3327" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M17.002 14.456H19.0028" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M17.002 17.4573H19.0028" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M4.99707 14.456H6.9979" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M4.99707 17.4573H6.9979" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M1.99609 21.0037H22.0044" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M2.99609 21.0037V12C2.99609 11.4475 3.444 10.9996 3.99651 10.9996H6.99776" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '<path d="M21.0036 21.0037V12C21.0036 11.4475 20.5557 10.9996 20.0032 10.9996H17.002" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>\n' +
        '</svg>'
  }
];

export default {
  tabs
}