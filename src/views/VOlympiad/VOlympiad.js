import VItemCard from "@/components/VItemCard";
import IconBase from "@/components/icons/IconBase";
import IconListView from "@/components/icons/components/IconListView";
import IconTilesView from "@/components/icons/components/IconTilesView";
import IconGreySearch from "@/components/icons/components/IconGreySearch";
import IconLoad from "@/components/icons/components/IconLoad";
import pagination from "@/mixins/pagination";
import payment from "@/mixins/payment";

export default {
  name: 'VOlympiads',

  mixins: [pagination, payment],

  data() {
    return {
      selectedTab: 'teacher',
      listView: false,
      loadMore: false,
      olympiadList: [],
      searchStr: ''
    }
  },
  components: {
    VItemCard,
    IconBase,
    IconListView,
    IconTilesView,
    IconGreySearch,
    IconLoad
  },

  mounted() {
    // window.addEventListener('scroll', this.handleScroll);
    this.fetchData();
  },

  watch: {
    selectedTab() {
      this.update();
    },
  },

  methods: {
    fetchData() {
      this.$store.commit('setLoading', true);
      this.loadMore = true;
      let params = {};
      if (this.selectedTab === 'nearest') params.is_nearest = 1;
      else params.is_childhood = (this.selectedTab === 'children') ? 1 : 0;
      params.search_text = this.searchStr || null

      this.$http({
        url: this.$getAPI('olympiad.olympiads'),
        params: {
          page: this.currentPage,
          ...params
        }
      }).then((res) => {
        this.hasNext = res.hasNext;
        this.currentPage = res.currentPage;
        this.olympiadList = this.olympiadList.concat(res?.rows);
        this.setDataLayer();
      }).finally(() => {
        this.loadMore = false;
        this.$store.commit('setLoading', false)
      })
    },

    setDataLayer() {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: 'view_item_list',
        ecommerce: {
          items: this.olympiadList.map((el, index) => ({
            item_name: el.name,
            item_id: el.id,
            price: el.cost,
            currency: 'Тенге',
            item_brand: "Олимпиада",
            item_category: "Олимпиада",
            index: index,
            quantity: index
          }))
        }
      });
    },

    onSearch() {
      this.update();
    },

    async handleFetchMoreData() {
      this.currentPage += 1;
      await this.fetchData();
    },

    update() {
      this.olympiadList = [];
      this.currentPage = 1;
      this.fetchData();
    },

    handleClickRating() {
      const routerData = this.$router.resolve({ name: 'VRatingOlympiad' });
      window.open(routerData.href, '_blank');
    },

    handleClickRules(id) {
      const routerData = this.$router.resolve({ name: 'VRuleTest', params: { id } });
      window.open(routerData.href, '_blank');
    },

    handleClickStart(olympiad) {
      this.setEventDataLayer('select_item', olympiad);
      this.setEventDataLayer('view_item', olympiad);
      this.setEventDataLayer('add_to_cart', olympiad);
      this.$http({
        url: this.$getAPI('olympiad.getId', {
          id: olympiad.id
        }),
      }).then((res) => {
        if (res?.errorType === 402) {
          const api = 'payment.payOlympiad';
          this.showPaymentModal(olympiad.id, olympiad.cost, api,null, null, olympiad);
        } else if (!res.error) {
          this.$router.push({ name: 'VTestOlympiad', params: { olympiadId: res?.id } });
        }
      })
    },

    setEventDataLayer(type, olympiad) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: type,
        ecommerce: {
          items: [{
            item_name: olympiad.name,
            item_id: olympiad.id,
            price: olympiad.cost,
            currency: 'Тенге',
            item_brand: "Олимпиада",
            item_category: "Олимпиада",
            index: 1,
            quantity: 1
          }]
        }
      });
    },

    setEventPaymentDataLayer(type, paymentType, olympiad) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ ecommerce: null });
      window.dataLayer.push({
        event: type,
        payment_type: paymentType,
        ecommerce: {
          items: [{
            item_name: olympiad.name,
            item_id: olympiad.id,
            price: olympiad.cost,
            currency: 'Тенге',
            item_brand: "Олимпиада",
            item_category: "Олимпиада",
            index: 1,
            quantity: 1
          }]
        }
      });
    },

    handleSubmitApp(olympiadId) {
      this.$vGlModal.show({
        modalName: 'v-request-modal',
        modalTitle: 'Оставить заявку',
        component: 'v-request-modal',
        modalWidth: 570,
        componentProps: {
          handleSubmit: (form) => {
            this.$http({
              url: this.$getAPI('olympiad.requests'),
              method: 'POST',
              data: {
                olympiad_test_id: olympiadId,
                ...form
              }
            }).then((res) => {
              if (!res.error) {
                this.$notify({
                  type: 'success',
                  title: 'Успешно',
                  text: 'Успешно отправлен',
                });
                this.$vGlModal.hide('v-request-modal');
                this.update();
              }
            })
          }
        }
      })
    },

    handleScroll() {
      let height = window.scrollY;
      if(height > 60) {
          document.getElementById('site-header').classList.remove('light');
      } else {
          document.getElementById('site-header').classList.add('light');
      }
    }
  }
}
