import VCarousel from 'components/VCarousel';
import 'vue-slick-carousel/dist/vue-slick-carousel.css';
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css';

export default {
  name: 'VBooks',

  components: {
    VCarousel
  },

  mounted() {
    this.fetchBooks();
    this.fetchBanner()
  },

  data() {
    return {
      booklist: [],
      bannerList: [],
    }
  },

  methods: {
    fetchBooks() {
      this.$http({
        url: this.$getAPI('book.books', {}),
      }).then((res) => {
        this.booklist = res.rows;
      })
    },
    fetchBanner() {
      this.$http({
        url: this.$getAPI('banner.banners', {}),
        params: {
          sectionId: 165153
        }
      }).then((res) => {
        this.bannerList = res[0];
      })
    },
    toDetailPage(book) {
      this.$router.push(book.url);
    },
  }
}
