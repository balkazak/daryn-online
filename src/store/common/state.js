export default {
  lang: window.localStorage.getItem('language') || 'kz',
  isOverlay: false,
  loading: false,
  visibleMedia: false,
  mediaSrc: null,
  spinnerLoader: false,
  isDark: false
}