import VPagination from '@/components/VPagination'
import IconBase from "@/components/icons/IconBase";
import IconEye from "@/components/icons/components/IconEye";
import IconPencil from "@/components/icons/components/IconPencil";
import IconHeart from "@/components/icons/components/IconHeart";
import VSelect from "@/components/VCustomSelect";
import pagination from "@/mixins/pagination";
import payment from "@/mixins/payment";

export default {
  name: 'VArticle',

  components: {
    VPagination,
    IconBase,
    IconEye,
    IconHeart,
    VSelect,
    IconPencil,
  },

  mixins: [pagination, payment],

  data() {
    return {
      articleList: [],
      degreeList: [],
      categoryList: [],
      sourceEvidence: '',
      sourceArticle: '',
      lightGallery: null,
      form: {
        search: '',
        degree: {
          name: "Все",
          id: null
        },
        sortBy: {
          name: "Все",
          value: null
        },
        category: {
          name: "Все",
          id: null
        }
      },
    }
  },

  created() {
    this.fetchCertificates();
  },

  mounted() {
    this.fetchDictionary();
    this.fetchData();
    window.lightGallery(document.getElementById('lightgallery'), {
      speed: 500,
    });
  },

  // beforeDestroy() {
  //   this.lightGallery.remove();
  // },

  watch: {
    form: {
      deep: true,
      handler: function () {
        this.fetchData();
      }
    }
  },

  computed: {
    getSortList() {
      return [
        { name: 'Все', value: null },
        { name: 'По дате', value: 'date' },
        { name: 'Самые читаемые', value: 'views' },
        { name: 'По лайку', value: 'likes' }
      ]
    }
  },

  methods: {
    fetchCertificates() {
      this.$http({
        url: this.$getAPI('dictionary.infoFiles')
      }).then(res => {
        this.sourceEvidence = Array.isArray(res) ? res[0]?.source : '';
        this.sourceArticle = Array.isArray(res) ? res[1]?.source : '';
      });
    },

    getForm() {
      return {
        degree_id: this.form.degree?.id,
        article_category_id: this.form.category?.id,
        sortBy: this.form.sortBy?.value,
        search: this.form.search || null
      }
    },

    fetchData() {
      const form = this.getForm();
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('article.articles'),
        params: {
          page: this.currentPage,
          ...form
        }
      }).then(res => {
        this.setDataArticles(res);
      }).finally(() => {
        this.$store.commit('setLoading', false);
      })
    },

    setDataArticles(response) {
      this.articleList = response?.rows.map(el => {
        return {
          authorName: el.author?.name,
          authorDegree: el.author?.degree,
          ...el
        }
      });
      this.lastPage = response?.lastPage;
      this.currentPage = response.currentPage;
    },

    fetchDictionary() {
      this.$http({
        url: this.$getAPI('dictionary.categories')
      }).then(res => {
        this.categoryList = res.article_categories;
        this.categoryList.unshift({ name: 'Все', id: null })
      });

      this.$http({
        url: this.$getAPI('dictionary.degrees')
      }).then(res => {
        this.degreeList = res.degrees;
        this.degreeList.unshift({ name: 'Все', id: null });
      });
    },

    changePage(page) {
      this.currentPage = page;
      this.fetchData();
    },

    async handleClickLike(id, index) {
      const oldVal = this.articleList[index].isLiked;
      this.articleList[index].isLiked = !this.articleList[index].isLiked;

      try {
        await this.$http({
          url: this.$getAPI('article.like', {
            id
          }),
          method: 'POST'
        });
        await this.fetchData();
      } catch (error) {
        this.articleList[index].isLiked = oldVal;
      }
    },

    handleOpenCertificate(article) {
      if (article.isHasCertificate) {
        this.getCertificateById(article.id);
      } else {
        const api = 'payment.payCertificate';
        this.showPaymentModal(article.id, article.cost, api);
      }
    },

    handleClickEdit(articleId) {
      this.$router.push({
        name: 'VProfileArticleEdit',
        query: { articleId: articleId }
      });
    },
  }
}
