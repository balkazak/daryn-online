import VPagination from '../../../components/VPagination'
import pagination from "@/mixins/pagination";

export default {
  name: 'VHistoryTransaction',

  mixins: [pagination],

  components: {
    VPagination
  },

  data() {
    return {
      transactions: []
    }
  },

  async mounted() {
    await this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('transaction.transactions'),
          params: {
            page: this.currentPage,
          }
        });
        this.transactions = res?.rows;
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
      } finally {
        this.$store.commit('setLoading', false);
      }
    },

    async changePage(page) {
      this.currentPage = page;
      await this.fetchData()
    }
  }
}