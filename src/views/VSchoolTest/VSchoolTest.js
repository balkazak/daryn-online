import IconBase from "@/components/icons/IconBase";
import IconCourseList from "@/components/icons/components/IconCourseList";
import IconCourseGrid from "@/components/icons/components/IconCourseGrid";
import VSchoolTestCard from "@/views/VSchoolTest/VSchoolTestCard";
import IconSubject from "@/components/icons/components/IconSubject";

export default {
  name: 'VSchoolTest',

  components: {
    IconBase,
    IconCourseList,
    IconCourseGrid,
    VSchoolTestCard,
    IconSubject
  },

  data() {
    return {
      listView: true,
      tests: [],
      subjects: [],
      grades: [],
      form: {
        object_id: '',
        language: 'kk',
        grades: [],
      },
      filterShowed: false,
    }
  },

  created() {
    this.fetchTestsData();
  },

  methods: {
    onClickAway(event) {
      console.log(event)
    },
    async fetchTestsData() {
      const form = this.getForm();
      const res = await this.$http({
        url: this.$getAPI('schoolTest.schoolTests'),
        params: {
          ...form
        }
      });
      this.tests = res?.tests;
      this.subjects = res?.objects;
      this.grades = res?.grades;
    },

    getForm() {
      return {
        object_id: this.form.object_id || null,
        grades: this.form.grades || null,
        language: this.form.language || null,
      }
    },

    async startTest(test) {
      const res = await this.$http({
        url: this.$getAPI('schoolTest.getId', { id: test.objectSubjectId}),
      });
      if (res.errorType === 404) {
        this.$notify({
          type: 'error',
          title: '',
          text:'Не найдено',
        });
      } else {
        this.$router.push({ name: 'VSchoolTestQuestion', params: { userTestId: res.userTestId } });
      }
    }
  },

  watch: {
    form: {
      deep: true,
      handler: function () {
        this.fetchTestsData();
      }
    }
  },
}
