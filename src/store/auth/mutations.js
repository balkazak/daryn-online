export default {
  auth(state, token) {
    state.token = token;
  },
  logout(state) {
    state.token = '';
    state.user = '';
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('user');
    window.localStorage.clear();
  },
  setProfile(state, payload) {
    state.user = payload;
    window.localStorage.setItem('user', JSON.stringify(payload));
    window.dataLayer.push({ user_id:  payload.user_id})
  },
  setTokenRefreshStatus(state, status) {
    /*
    * status: String (loading, loaded, reject)
    * */
    state.tokenRefreshStatus = status;
  },
}