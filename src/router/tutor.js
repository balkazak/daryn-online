import errorHandler from "../helpers/errorHandler";

export default [
  {
    path: 'tutor',
    name: 'VTutor',
    component: () => import('../views/VTutor').catch(errorHandler),
  },
    {
        path: 'tutor/:id',
        name: 'VSingleTutor',
        component: () => import('../views/VTutor/VSingleTutor').catch(errorHandler),
        meta: {
            authorization: true,
        },
    },
]
