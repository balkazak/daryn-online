import VSelect from "@/components/VCustomSelect";
import pagination from "@/mixins/pagination";
import VSchoolCard from './VSchoolCard'
import VPagination from '../../components/VPagination';


export default {
  name: 'VSchool',

  components: {
    VSelect,
    VSchoolCard,
    VPagination
  },

  mixins: [pagination],

  data() {
    return {
      regionList: [],
      categoryList: [],
      school: {
        name: "Все",
        id: null
      },
      region: {
        name: "Все",
        id: null
      },
      allSchools: [],
      hasNext: false,
      cityList: [],
      currentPage: 0,
      lastPage: 0
    }
  },

  mounted() {
    this.fetchAllSchools();
    this.getCities();
    this.getCategories();
    this.translate();
  },

  watch: {
    region: {
      deep: true,
      async handler() {
        await  this.fetchAllSchools();
      }
    },
    school: {
      deep: true,
      async handler() {
        await  this.fetchAllSchools();
      }
    },
  },

  methods: {
    translate() {
      this.region.name = this.trans('general.all');
    },
    async fetchAllSchools() {
      this.$store.commit('setSpinnerLoader', true);
      try {
        const form = this.getForm();
        const res = await this.$http({
          url: this.$getAPI('schools.getSchools'),
          params: {
            page: this.currentPage,
            ...form
          }
        });
        this.allSchools = res?.rows;
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
        this.hasNext = res?.hasNext;
      } catch (error) {
        console.error(error);
      } finally {
        this.$store.commit('setSpinnerLoader', false);
      }
    },
    async getCategories() {
      this.$store.commit('setSpinnerLoader', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('schools.categories'),
          params: {
            page: this.currentPage,
          }
        });
        this.categoryList = res.rows
        this.categoryList.unshift({
          'name': this.trans('general.all')})
      } catch (error) {
        console.error(error);
      } finally {
        this.$store.commit('setSpinnerLoader', false);
      }
    },
    getForm() {
      return {
        category_id: this.school?.id,
        region_id: this.region?.id
      }
    },
    async changePage(page) {
      this.currentPage = page;
      await this.fetchAllSchools(page)
    },
    async getCities() {
      this.$store.commit('setLoading', true);
      await this.$http({
        url: this.$getAPI('dictionary.regions'),
        params: {
          is_speciality: 1
        }
      }).then(res => {
        this.cityList = res.regions
        this.cityList.unshift({
          'name': this.trans('modules.speciality.all_cities')})
      }).finally(() => {
        this.$store.commit('setLoading', false);
      })
    },
    goToSchoolPage(item) {
      this.$router.push({
        name: 'VSingleSchool',
        params: {
          id: item
        }
      })
    },
    gotoTrial () {
      let routeData = this.$router.resolve({name: 'VTrial'});
      window.open(routeData.href, '_blank');
    },
    gotoNavigator () {
      let url = "https://edunavigator.kz/ru?utm_source=darynonline&utm_medium=banner&utm_campaign=darynonlinespring"
      window.open(url, '_blank');
    }
  },


}
