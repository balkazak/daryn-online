import VTutorCard from "./components/VTutorCard";
import VSelect from "../../components/VCustomSelect";
import VInput from "../../components/VCustomInput";
import VPagination from "../../components/VPagination";
import pagination from "@/mixins/pagination";
import IconBase from "@/components/icons/IconBase";
import IconToggle from "@/components/icons/components/IconToggle";


export default {
  name: 'VTutor',

  mixins: [pagination],

  data() {
    return {
      subjectDict: [],
      regionDict: [],
      languageDict: [],
      sortes: [
        {label: 'Сортировать по рейтингу', value: 'rating'},
        {label: 'Сортировать по цене', value: 'price_desc'},
        {label: 'Сортировать по опыту', value: 'last'}
      ],
      selectedSort: {
        value: null
      },
      selectedSubject: { id: null },
      selectedLanguage: { id: null },
      selectedRegion: { id: null },
      priceFrom: null,
      priceTo: null,
      filterOpened: false,
      tutorList: [],
      page: null,
      filterParams: {
        subject_id: null,
        language_id: null,
        region_id: null,
        price_from: null,
        price_to: null,
        sortBy: null
      },
    }
  },
  watch: {
    selectedSubject() {
      this.filterParams.subject_id = this.selectedSubject?.id || null;
    },
    selectedLanguage() {
      this.filterParams.language_id = this.selectedLanguage?.id;
    },
    selectedRegion() {
      this.filterParams.region_id = this.selectedRegion?.id;
    },
    selectedSort() {
      this.filterParams.sortBy = this.selectedSort?.value;
    }
  },
  async mounted() {
    await this.fetchDictionary();
    await this.fetchData();
  },
  components: {
    VTutorCard,
    VSelect,
    VInput,
    VPagination,
    IconBase,
    IconToggle
  },
  methods: {
    toggleFilter() {
      this.filterOpened = !this.filterOpened
    },
    async changePage(page) {
      this.currentPage = page;
      await this.fetchData(this.currentPage);
    },
    async fetchData(page = 1){
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('tutors.tutors'),
          params: {
            page: page
          }
        });
        this.tutorList = res.rows;
        this.lastPage = res.lastPage;
        this.currentPage = res.currentPage;
      } catch (error) {
        console.error(error);
      } finally {
        this.$store.commit('setLoading', false);
      }
    },
    fetchDictionary() {
      this.$http({ url: this.$getAPI('tutors.subjectDict') }).then(res => {
        this.subjectDict = res.rows;
      });
      this.$http({ url: this.$getAPI('tutors.languageDict') }).then(res => {
        this.languageDict = res.rows;
      });
      this.$http({ url: this.$getAPI('tutors.regionDict') }).then(res => {
        this.regionDict = res.rows;
      });
    },
    async filterByParams() {
      try {
        const res = await this.$http({
          url: this.$getAPI('tutors.tutors'),
          params: this.filterParams
        });
        this.tutorList = res.rows;
      } catch (error) {
        console.error(error);
      }
    },
    clearFilterParams() {
      Object.keys(this.filterParams).forEach(k => this.filterParams[k] = null);
      this.selectedSubject = this.selectedLanguage = this.selectedRegion = this.selectedSort = null;
      this.$refs.sortSelect.clearSelection();
      this.$refs.languageSelect.clearSelection();
      this.$refs.regionSelect.clearSelection();
      this.$refs.subjectSelect.clearSelection();
    }
  }
}
