import errorHandler from "../helpers/errorHandler";
import render from "@/helpers/renderRouterView";


export default [
  {
    path: 'trial',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VTrial',
        component: () => import('../views/VTrial').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/:trialTestId',
        name: 'VTrialTest',
        component: () => import('../views/VTrial/VTrialTest').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'rule-test/:id',
        name: 'VTrialRuleTest',
        component: () => import('../views/VTrial/VRuleTest').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/result/:trialTestId',
        name: 'VResultTrial',
        component: () => import('../views/VTrial/VResult').catch(errorHandler),
        meta: {
          authorization: true,
        },
      }
    ]
  },

]