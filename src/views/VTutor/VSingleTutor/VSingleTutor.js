import VueSlickCarousel from 'vue-slick-carousel'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
// optional style for arrows & dots
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'
import StarRating from 'vue-star-rating'
import IconBase from "@/components/icons/IconBase";
import IconSubjects from "@/components/icons/components/IconSubjects";
import IconLocation from "@/components/icons/components/IconLocation";
import IconRegion from "@/components/icons/components/IconRegion";
import IconOnline from "@/components/icons/components/IconOnline";
import IconDeparture from "@/components/icons/components/IconDeparture";
import IconBluePhone from "@/components/icons/components/IconBluePhone";
import IconAtSymbol from "@/components/icons/components/IconAtSymbol";
import IconBlueWhatsapp from "@/components/icons/components/IconBlueWhatsapp";
import IconReviews from "@/components/icons/components/IconReviews";
import IconLittleReviews from "@/components/icons/components/IconLittleReviews";
import IconRatingStar from "@/components/icons/components/IconRatingStar";
export default {
  name: 'VSingleTutor',

  data() {
      return {
        tutor: null,
        activeItem: 'subject1',
        show: true,
          days: ['06:00', '09:00', '12:00', '15:00', '18:00', '21:00'],
          sameTutorsSlider: {
              dots: true,
              infinite: true,
              speed: 700,
              slidesToShow: 3,
              slidesToScroll: 3,
              arrows: false,
              responsive: [
                  {
                      breakpoint: 576,
                      settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                          autoplay: false,
                      }
                  }
              ]
          },

      }
  },
    async created() {
      if (this.$route.params.id) {
          await this.fetchTutorById();
      }
    },
  components: {
    VueSlickCarousel,
    StarRating,
    IconSubjects,
    IconBase,
    IconLocation,
    IconRegion,
    IconOnline,
    IconDeparture,
    IconBluePhone,
    IconAtSymbol,
    IconBlueWhatsapp,
    IconReviews,
    IconLittleReviews,
    IconRatingStar
  },
    methods: {
        handleSelectCheckbox(id) {
            this.selectedCheckboxId = id;
        },
        async fetchTutorById() {
            try {
                const res = await this.$http({
                    url: this.$getAPI(`tutors.singleTutor`,
                        {
                            id: this.$route.params.id
                        }),
                    });
                this.tutor = res;
            } catch (error) {
                console.error(error);
            }
        },
        isActive (menuItem) {
            return this.activeItem === menuItem
        },
        setActive (menuItem) {
            this.activeItem = menuItem
        },
        showMore() {
          this.show = !this.show
        },
        getRelatedSubjects(subjects) {
            let subjectNames = subjects[0].object_name_kz;
            for (let i = 1; i < subjects.length; i++) {
                subjectNames += ', ' + subjects[i].object_name_kz
            }
            return subjectNames;
        },
        getSchedule(day, time) {
            let days = [];
            for (let i = 0; i < this.tutor.schedule.length; i++) {
                if (day === this.tutor.schedule[i].time.day) {
                    days.push(this.tutor.schedule[i].time.time_from.substring(0, 5));
                }
            }
            for (let i = 0; i < days.length; i++) {
                if (time === days[i]) {
                    return true
                }
            }
            return false;
        }
    }
}
