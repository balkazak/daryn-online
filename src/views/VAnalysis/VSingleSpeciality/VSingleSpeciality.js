export default {
    name: "VSingleSpeciality",

    props: {
        item: Object
    },

    methods: {
        async goToSpecialityAbout(item) {
            await this.$store.commit('setSpecialitySingle', { specialitySingle: item })
            this.$router.push({
                name: 'VSingleAnalysis',
                params: {
                    id: item.url
                }
            })

            // let routeData = this.$router.resolve({name: 'VSingleAnalysis', params: {id: item.id}});
            // window.open(routeData.href, '_blank');
        },
        getClasses(item) {
            switch (item.demand) {
                case 'Орташа':
                case 'Средняя':
                    return 'warning'
                case 'Жоғары':
                case 'Высокая':
                    return 'success'
                default:
                    return 'danger'
            }
        }
    }
}
