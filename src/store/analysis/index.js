
export default {

    state: {
        probabilityResult : [],
        specialitySingle: [],
        userPlanInfo: {
            name: "Все",
            id: null
        },
    },

    actions: {

    },

    mutations: {
        setProbabilityResult(state, { probabilityResult }) {
            state.probabilityResult = probabilityResult
        },
        setSpecialitySingle(state, { specialitySingle }) {
            state.specialitySingle = specialitySingle
        },
        setUserPlan(state, { userPlanInfo }) {
            state.userPlanInfo = userPlanInfo
        },
    },

    getters: {
        getProbabilityResult(state) {
            return state.probabilityResult
        },
        getSpecialitySingle(state) {
            return state.specialitySingle
        },
        getUserPlan(state) {
            return state.userPlanInfo
        },
    }

}
