import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';

export default [
  {
    path: 'article',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VArticle',
        component: () => import('../views/VArticle').catch(errorHandler),
        meta: {
          authorization: false,
        },
      },
      {
        path: ':id',
        name: 'VArticleMore',
        component: () => import('../views/VArticle/VArticleMore').catch(errorHandler),
        meta: {
          authorization: false,
        },
      },
    ]
  }
]