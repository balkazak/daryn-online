import errorHandler from "../helpers/errorHandler";
import render from "../helpers/renderRouterView";

export default [
    {
        path: 'search',
        component: {
            render
        },
        children: [
            {
                path: '',
                name: 'VGlobalSearch',
                component: () => import('../views/VGlobalSearch').catch(errorHandler),
                meta: {
                    authorization: false,
                },
            },
        ]
    }
]
