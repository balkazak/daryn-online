import VProfileCard from "./components/VProfileCard";
import profileList from "../../jsons/profileMenu";
import { TransitionExpand } from 'vue-transition-expand';

export default {
  name: 'VProfile',

  components: {
    VProfileCard,
    TransitionExpand
  },

  data() {
    return {
      isProfileAside: false,
      profileMenu: profileList,
    }
  },

  computed: {
    showProfile() {
      return this.$screen.gt.md || (this.$screen.lt.lg && this.isProfileAside);
    }
  },

  methods: {
    checkActiveMenu(item) {
      return this.$route.name === item.name;
    },

    handleRouteLink(item) {
      if (item.isExternal) {
        window.location.href = item.url;
      } else {
        this.$router.push({ name: item.name })
      }
    },
  }
}