import axios from 'axios';
import store from '../store';

export const instance = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? '' : process.env.VUE_APP_SERVER_URL,
});

instance.defaults.headers.common = {
  Accept: 'application/json',
  'X-Localization': store.state.common.lang,
  'Content-Type': 'application/json',
};

// В каждый запрос добавляем токен если он есть
instance.interceptors.request.use((config) => {
    const token = store.state.auth.token;
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error),
);

instance.interceptors.response.use(async(res) => {
  return (res && res.data) || res;
},  (error) => Promise.reject(error));
