export default {
  interfaceLang: (state) => state.lang,
  isOverlay: (state) => state.isOverlay,
  loading: (state) => state.loading,
  visibleMedia: (state) => state.visibleMedia,
  getMediaSrc: (state) => state.mediaSrc,
  spinnerLoader: (state) => state.spinnerLoader,
  isDark: (state) => state.isDark
}