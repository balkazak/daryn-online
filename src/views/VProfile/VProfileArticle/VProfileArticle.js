import IconBase from "@/components/icons/IconBase";
import IconPen from "@/components/icons/components/IconPen";
import IconPencil from "@/components/icons/components/IconPencil";
import IconCloudDownload from "@/components/icons/components/IconCloudDownload";
import IconHeart from "@/components/icons/components/IconHeart";
import IconEye from "@/components/icons/components/IconEye";
import IconSpin from "@/components/icons/components/IconSpin";
import IconTrash from "@/components/icons/components/IconTrash";
import payment from "@/mixins/payment";

export default {
  name: 'VProfileArticle',

  components: {
    IconBase,
    IconPen,
    IconPencil,
    IconCloudDownload,
    IconHeart,
    IconEye,
    IconSpin,
    IconTrash
  },

  mixins: [payment],

  data() {
    return {
      articleList: [],
      hasNext: false,
      loadMore: false
    }
  },

  created() {
    this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('article.myArticle')
        });
        this.articleList = res.rows.map(el => {
          return {
            ...el,
            authorAvatar: el.author.avatar,
            authorDegree: el.author.degree,
            authorName: el.author.name
          }
        });
        this.hasNext = res.hasNext;
      } finally {
        this.$store.commit('setLoading', false);
      }
    },

    handleClickCreate() {
      this.$router.push({name: 'VProfileArticleCreate'});
    },

    handleClickEdit(articleId) {
      this.$router.push({
        name: 'VProfileArticleEdit',
        query: { articleId: articleId }
      });
    },

    async handleClickDelete(articleId) {
      const res = await this.$http({
        url: this.$getAPI('article.remove', {
          id: articleId
        }),
        method: 'DELETE',
      });

      if (!res.error) {
        this.$notify({
          type: 'success',
          title: 'Успешно',
          text: res?.message,
        });
        this.articleList = this.articleList.filter(el => el.id !== articleId);
      }
    },

    async handleClickLike(id, index) {
      const oldVal = this.articleList[index].isLiked;
      this.articleList[index].isLiked = !this.articleList[index].isLiked;

      try {
        const res = await this.$http({
          url: this.$getAPI('article.like', {
            id
          }) ,
          method: 'POST'
        });
        if (!res.error) {
          this.$notify({
            type: 'success',
            title: 'Успешно',
            text: res?.message,
          });
          await this.fetchData();
        }
      } catch (error) {
        this.articleList[index].isLiked = oldVal;
      }
    },

    handleOpenCertificate(article) {
      if (article.isHasCertificate) {
        this.getCertificateById(article.id);
      } else {
        const api = 'payment.payCertificate';
        this.showPaymentModal(article.id, article.cost, api);
      }
    },
  }
}