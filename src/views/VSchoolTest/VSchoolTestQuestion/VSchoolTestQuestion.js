import VRadioGroup from "components/VRadioGroup/VRadioGroup";
import {isNumber} from "lodash";
export default {
  name: 'VSchoolTestQuestion',

  components: {
    VRadioGroup,
  },

  data() {
    return {
      questions: [],
      timer: '',
      points: 0,
      currentQuestion: {},
      selectedAnswer: null,
      showCorrect: false,
    }
  },

  created() {
    this.fetchQuestionsData();
  },

  computed: {
    currentQuestionTitle() {
      return this.currentQuestion.question || '';
    },
    isLastQuestion() {
      const lastIdx = this.questions?.length - 1;
      const idx = this.questions?.findIndex(el => el.questionId === this.currentQuestion.questionId)
      return idx === lastIdx;
    },
    getVariants() {
      return this.currentQuestion.variants;
    },
    isAnswered() {
      return this.currentQuestion.isAnswered;
    },
  },

  methods: {
    fetchQuestionsData() {
      const { userTestId } = this.$route.params;
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('schoolTest.getQuestions', { userTestId })
      }).then(res => {
        if (!res.error) this.setData(res);
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setData(res) {
      this.questions = Object.values(res.questions)
      this.questions.forEach(question => { question.status = '' });
      this.setDefaultCurrentQuestion();
      this.togglePagination();
      this.clearAnswer();
    },

    setDefaultCurrentQuestion() {
      this.currentQuestion = this.questions.find(el => el.isAnswered === false) || this.questions[0];
    },

    handleSelectQuestion(idx) {
      if (!this.questions[idx]) {
        this.handleFinish();
      } else {
        this.currentQuestion = this.questions[idx];
        this.togglePagination();
        this.clearAnswer();
      }
    },

    togglePagination() {
      this.questions.forEach((el) => {
        const idx = el.status.search('info');
        el.status = el.status.slice(0, idx);

        if (el.questionId === this.currentQuestion.questionId) el.status = 'info';
      });
    },

    clearAnswer() {
      this.selectedAnswer = null;
    },

    changeAnswer(answerId) {
      this.selectedAnswer = answerId;
      this.currentQuestion.userAnswers = [answerId];
    },

    handleNextQuestion() {
      const index = this.questions.findIndex(el => el.questionId === this.currentQuestion.questionId);
      if (index !== -1) {
        const nextQuestionIdx = index + 1;
        this.handleSelectQuestion(nextQuestionIdx);
      }
    },

    async handleCheck() {
      if (this.selectedAnswer) {
        await this.sendRequest();
      } else {
        this.$notify({
          type: 'error',
          title: '',
          text: this.trans('general.choose_answer'),
        });
      }
    },

    handleFinish() {
      const { userTestId } = this.$route.params;
      this.$store.commit('setLoading', true);
      this.$http({
        url: this.$getAPI('schoolTest.finish', {
          userTestId
        }),
        method: 'POST'
      }).then(() => {
        this.$router.push({name: 'VSchoolTestResult', params: { userTestId }});
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    async sendRequest() {
      const { userTestId } = this.$route.params;
      const { questionId } = this.currentQuestion;
      this.$store.commit('setLoading', true);
      this.$http({
        method: 'POST',
        url: this.$getAPI('schoolTest.sendAnswer', {
          userTestId,
          questionId
        }),
        data: { answer: this.selectedAnswer }
      }).then((res) => {
        if (!res.error) {
          this.currentQuestion.isAnswered = true;
          this.points += isNumber(res.point) ? res.point : 0;
          this.handleNextQuestion();
        }
      }).finally(() => {
        this.$store.commit('setLoading', false);
      });
    },

    getStyle(question) {
      if (question.isAnswered) {
        return {
          cursor: 'default',
          background: '#f5f5f5',
          borderColor: '#f5f5f5',
          color: 'inherit !important'
        }
      }
      return { cursor: 'default', }
    },
  },
}