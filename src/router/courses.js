import errorHandler from "../helpers/errorHandler";
import render from "@/helpers/renderRouterView";

export default [
  {
    path: 'courses',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VCourses',
        component: () => import('../views/VCourses').catch(errorHandler),
      },
      {
        path: ':id',
        name: 'VSingleCourse',
        component: () => import('../views/VCourses/VSingleCourse').catch(errorHandler),
        meta: {
            authorization: true,
        },
      },
      {
        path: ':id/lesson/:lessonId',
        name: 'VLessonCourse',
        component: () => import('../views/VCourses/VLessonCourse').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'test/:testId',
        name: 'VCourseTest',
        component: () => import('../views/VCourses/VCourseTest').catch(errorHandler),
        meta: {
          authorization: true,
        },
      },
      {
        path: 'result/:testId',
        name: 'VCourseResult',
        component: () => import('../views/VCourses/VCourseResult').catch(errorHandler),
        meta: {
          authorization: true,
        },
      }
    ]
  }
]
