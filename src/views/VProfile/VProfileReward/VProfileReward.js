import VPagination from '../../../components/VPagination'
import pagination from "@/mixins/pagination";

export default {
  name: 'VProfileReward',

  mixins: [pagination],

  components: {
    VPagination
  },

  data() {
    return {
      certificateList: [],
    }
  },

  async mounted() {
    await this.fetchData();
  },

  methods: {
    async fetchData() {
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI('reward.rewards'),
          params: {
            page: this.currentPage,
          }
        });
        this.certificateList = res?.rows.filter(item => item.link !== null);
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
      } finally {
        this.$store.commit('setLoading', false);
      }
    },

    async changePage(page) {
      this.currentPage = page;
      await this.fetchData()
    }
  }
}