export default [
  {
    title: 'menu.profile',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile`
  },
  {
    title: 'menu.conference',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/stream`,
  },
  {
    title: 'menu.basket',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/basket`,
  },
  {
    title: 'menu.combo_busket',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/basket/combo`,
  },
  {
    title: 'menu.add_money',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/purse`,
  },
  {
    title: 'menu.history_payment',
    isExternal: false,
    url: `/profile/transaction`,
  },
  {
    title: 'menu.test_results',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/test`,
  },
  {
    title: 'menu.referral',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/referral`,
  },
  {
    title: 'menu.profile_edit',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/edit`,
  },
  {
    title: 'menu.change_password',
    isExternal: true,
    url: `${process.env.VUE_APP_DARYN_URL}/profile/password/edit`,
  },
  {
    title: 'Выйти',
    url: 'logout'
  }
]