const path = require('path');

module.exports = {
  productionSourceMap: false,
  assetsDir: 'spa',
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        components: path.resolve(__dirname, 'src/components')
      }
    }
  },
  devServer: {
    proxy: {
      "^/api": {
        target: "https://localhost:8080",
        ws: true,
        changeOrigin: true,
      },
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    },
    public: "http://localhost:8080",
    disableHostCheck: true,
  },
};
