import VFavoriteCard from "../components/VFavoriteCard";

export default {
  name: 'VProfileFavorite',

  components: {
    VFavoriteCard
  },

  data() {
    return {
      favoriteList: []
    }
  },
  async created() {
    await this.fetchData();
  },

  methods: {
    async fetchData() {
      try {
        const res = await this.$http({
          url: this.$getAPI('favorite.favorites')
        });
        this.favoriteList = res.rows;
      } catch (error) {
        console.error(error);
      }
    },
    async deleteFavoriteItem(item) {
      let index = this.favoriteList.indexOf(item);
      if (index !== -1) {
        this.favoriteList.splice(index, 1);
      }
      try {
        await this.$http({
          url: this.$getAPI('favorite.deleteOrAddFavorite', {
            subject_id: item.id
          }),
          method: 'POST'
        });
        this.$notify({
          type: 'success',
          title: 'Успешно',
          text: 'Успешно удален из избранных!',
        });
        await this.fetchData();
      } catch (error) {
        console.error(error);
      }
    },
  }
}
