import VGlModalWrapper from './components/VGlModalWrapper';
import VGlModal from './components/VGlModal';

export default {
  install(Vue) {
    if (!Vue.prototype.$eventHub) {
      Vue.prototype.$eventHub = new Vue();
    }

    Vue.prototype.$vGlModal = {
      show(obj = {
        modalName: '',
        modalLocked: false,
        modalTitle: '',
        modalClasses: '',
        modalCustom: false,
        modalOverlayDisable: false,
        modalWidth: 0,
        component: '',
        componentProps: {},
      }) {
        Vue.prototype.$eventHub.$emit('v-gl-modal--show', obj);
      },
      update(name, obj) {
        Vue.prototype.$eventHub.$emit(`v-gl-modal--${name}-update`, obj);
      },
      hide(name) {
        Vue.prototype.$eventHub.$emit('v-gl-modal--hide', name);
      },
      hideAll() {
        Vue.prototype.$eventHub.$emit('v-gl-modal--hide-all');
      },
    };

    Vue.component('v-gl-modal-wrapper', VGlModalWrapper);
    Vue.component('v-gl-modal', VGlModal);
  },
};
