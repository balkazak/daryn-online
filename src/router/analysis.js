import errorHandler from "../helpers/errorHandler";
import render from "../helpers/renderRouterView";

export default [
    {
        path: 'speciality',
        component: {
            render
        },
        children: [
            {
                path: '',
                name: 'VAnalysis',
                component: () => import('../views/VAnalysis').catch(errorHandler),
                meta: {
                    authorization: true,
                },
            },
            {
                path: ':id',
                name: 'VSingleAnalysis',
                component: () => import('../views/VAnalysis/VSingleSpeciality/VSingleSpecialityPage').catch(errorHandler),
                meta: {
                    authorization: true,
                },
            },
            {
                path: 'plan',
                name: 'VAnalysisPlan',
                component: () => import('../views/VAnalysis/VAnalysisPlan').catch(errorHandler),
                meta: {
                    authorization: true,
                },
            },
        ]
    }
]
