import Vue from 'vue';
import VueRouter from "vue-router";
import render from '@/helpers/renderRouterView';
import store from '../store';
import request from "../services/request";
import API from '../services/api';
import { getCookie } from '../helpers/cookie';
import profile from "./profile";
import authenticate from "./authenticate";
import olympiad from "./olympiad";
import tutor from "./tutor";
import courses from "./courses";
import ent from "./ent"
import article from "./article";
import school from "./school";
import analysis from "./analysis";
import belbin from "./belbin";
import trial from "./trial";
import attestation from "./attestation";
import contacts from "./contacts";
import faq from "./faq";
import schoolTest from "./schoolTest";
import freeProduct from "./freeProduct";
import globalSearch from "./globalSearch";
import books from "@/router/books";
// import VGlModal from '@/components/VGlModal';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VMainPage',
        component: () => import('../views/VMainPage')
      },
      ...authenticate,
      ...profile,
      ...olympiad,
      ...tutor,
      ...courses,
      ...ent,
      ...article,
      ...school,
      ...analysis,
      ...belbin,
      ...trial,
      ...attestation,
      ...contacts,
      ...faq,
      ...schoolTest,
      ...freeProduct,
      ...globalSearch,
      ...books,
      {
        path: '/*',
        name: 'VNotFoundPage',
        component: () => import('../views/VNotFoundPage'),
      },
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes
});

router.beforeEach(async (to, from, next) => {
  if (getCookie('g_old_user_id')
      && getCookie('g_old_token')
      && (Number(getCookie('g_old_user_id')) !== store.getters.getUserId))
  {
    const res = await request({
      url: API('auth.loginWithToken'),
      method: 'POST',
      data: {
        mobile_token: getCookie('g_old_token'),
        user_id: getCookie('g_old_user_id')
      }
    });
    if (!res.error) {
      await store.dispatch('auth', res?.access_token);
    }
  }

  if (store.getters.isLoggedIn) {
    store.commit('setLoading', true);
    try {
      const res = await request({
        url: API('auth.profile'),
      });
      if (!res.error) {
        store.commit('setProfile', res);
        if (res.editRole) {
          Vue.prototype.$vGlModal.show({
            modalName: 'v-info-registration-modal',
            modalTitle: 'Выберите роль',
            modalWidth: 970,
            component: 'v-info-registration-modal',
          })
        }
      } else {
        return store.dispatch('logout');
      }
    } finally {
      store.commit('setLoading', false);
    }
  }


  if (!store.getters.isLoggedIn) {
    // if (process.env.NODE_ENV === 'production') {
      document.cookie = "redirect_back=" + window.location.href + ";path=/;domain=daryn.online";
      // window.location.href = `${process.env.VUE_APP_DARYN_URL}/auth/login`;
    // } else {
      // next('/auth/login');
      // return this.$vGlModal.show({
      //   modalName: 'v-auth-modal',
      //   component: 'v-auth-modal',
      //   modalWidth: 970,
      // });
    // }
  }

  if (to.path === '/auth/login' && store.getters.isLoggedIn) {
    return next('/');
  }

  return next();
});

const TITLE_KAZ = 'Daryn.Online – жаңа ұрпаққа арналған білім беру жүйесі';
const TITLE_RUS = 'Daryn.online - образование для нового поколения'
router.afterEach(async (to) => {
  if (!['VAuth', 'VEmailConfirm'].includes(to.name)) {
    Vue.nextTick(() => {
      if(store.getters.interfaceLang === 'kz') {
        document.title = TITLE_KAZ;
      } else if (store.getters.interfaceLang === 'ru') {
        document.title = TITLE_RUS;
      }
      document.querySelector('meta[name="description"]')
          .setAttribute("content", TITLE_KAZ);
    });
    // const res = await request({
    //   url: API('menus.getMenuById', {
    //     url: to.fullPath.slice(1)
    //   })
    // });
  }
});

export default router;
