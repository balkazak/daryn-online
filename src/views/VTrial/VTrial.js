import IconBase from "@/components/icons/IconBase";
import IconLoad from "@/components/icons/components/IconLoad";
import pagination from "@/mixins/pagination";
import payment from "@/mixins/payment";
import VItemCard from "@/components/VItemCard";
import IconListView from "components/icons/components/IconListView";
import IconTilesView from "components/icons/components/IconTilesView";

export default {
  name: 'VTrial',

  mixins: [pagination, payment],

  data() {
    return {
      isActive: true,
      listView: false,
      loadMore: false,
      list: [],
      searchStr: '',
    }
  },
  components: {
    VItemCard,
    IconBase,
    IconListView,
    IconTilesView,
    IconLoad
  },

  mounted() {
    this.fetchList();
    if(window.innerWidth > 767 && window.innerWidth < 992) {
      this.listView = true
    }
  },

  methods: {
     fetchList(params = {}) {
      this.$http({
        url: this.$getAPI('trial.getTrialTests'),
        params: params,
      }).then((res) => {
        if (!res.error) this.list = res;
        if(this.$route.query.test_id){
          var new_result = [];
          new_result[0] = res.find(item => item.id === parseInt(this.$route.query.test_id))
          this.list = new_result;
        }
      })
    },

    onSearch() {
      const params = {
        search_text: this.searchStr || null
      }
      this.fetchList(params);
    },

    handleClickRules(id) {
      const routerData = this.$router.resolve({ name: 'VTrialRuleTest', params: { id } });
      window.location.href = routerData.href;
    },

    async handleClickStart(item) {
      switch (item.type) {
        case 'belbin':
          await this.$router.push({
            path: '/belbin',
            query: { lang: item.lang },
          });
          break;
        case 'attestation':
          await this.$router.push({
            path: '/attestation',
            query: { lang: item.lang },
          });
          break;
        case 'ent':
          await this.startEnt(item);
          break;
        default:
          await this.startTrialTest(item);
          break;
      }
    },

    async startEnt(ent) {
      const entId = ent.id;
      const cost = ent.cost;

      const res = await this.$http({
        url: this.$getAPI('ent.getId', { id: entId }),
      });

      if (res.errorType === 402) {
        const api = 'payment.payEnt';
        this.showPaymentModal(entId, cost, api, 'ent');
      } else if (res && res.isStarted) {
        this.$router.push({ name: 'VTest', params: { entId: res.id } });
      } else {
        this.$router.push({ name: 'VEnt', params: { entId: res.id } });
      }
    },

    async startTrialTest(trial) {
      const trialId = trial.id;
      const cost = trial.cost;
      const lang = trial.lang;

      const res = await this.$http({
        url: this.$getAPI('trial.getId', { id: trialId }),
        params: {
          lang: lang,
        }
      });
      if (res.errorType === 402) {
        const api = 'payment.payTrialTest';
        this.showPaymentModal(trialId, cost, api, 'trial',lang);
      } else {
        await this.$router.push({name: 'VTrialTest', params: {trialTestId: res.id}});
      }
    }
  }
}