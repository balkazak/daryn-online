import Vue from 'vue'

const filters = () => {
  Vue.filter('phoneNumber', function (value) {
    if (value && typeof value === 'string') {
        return '+' + value.replace(/[^0-9]/g, '');
    }

    return ''
  }),
  Vue.filter('whatsapp', function (value) {
      if (value && typeof value === 'string') {
          return 'https://wa.me/' + value.replace(/[^0-9]/g, '');
      }

      return ''
  }),
  Vue.filter('getImgSrc', function (value) {
      if (value && typeof value === 'string') {
         if (value.includes('https://')) {
            return value
         } else {
            return `${process.env.VUE_APP_DARYN_URL}/` + value
           }
         }
      return ''
  })
};

export default filters
