import errorHandler from "../helpers/errorHandler";
import render from "../helpers/renderRouterView";

export default [
    {
        path: 'faq',
        component: {
            render
        },
        children: [
            {
                path: '',
                name: 'VFaq',
                component: () => import('../views/VFaq').catch(errorHandler),
                meta: {
                    authorization: true,
                },
            }
        ]
    }
]
