
export default {

    state: {
        belbinResult : [],
    },

    actions: {

    },

    mutations: {
        setBelbinResult(state, { belbinResult }) {
            state.belbinResult = belbinResult
        }
    },

    getters: {
        getBelbinResult(state) {
            return state.belbinResult
        },
    }

}
