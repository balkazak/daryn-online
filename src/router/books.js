import errorHandler from "../helpers/errorHandler";
import render from '../helpers/renderRouterView';

export default [
  {
    path: 'books',
    component: {
      render
    },
    children: [
      {
        path: '',
        name: 'VBooks',
        component: () => import('../views/VBooks').catch(errorHandler),
        meta: {
          authorization: false,
        },
      },
      {
        path: ':url',
        name: 'VBookDetail',
        component: () => import('../views/VBooks/VBookDetail').catch(errorHandler),
        meta: {
          authorization: false,
        },
      },
    ]
  }
]