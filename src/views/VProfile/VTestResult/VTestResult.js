import VPagination from '../../../components/VPagination'
import pagination from "@/mixins/pagination";

export default {
  name: 'VTestResult',

  mixins: [pagination],

  components: {
    VPagination
  },

  data() {
    return {
      resultList: [],
      activeTab: 'olympiad',
      testTypeList: [
        {
          title: 'Олимпиада',
          key: 'olympiad',
        },
        {
          title: 'modules.test_results.tab.ent',
          key: 'ent',
        },
        {
          title: 'modules.test_results.tab.special',
          key: 'special',
        },
        {
          title: 'Аттестация',
          key: 'attestation',
        },
      ]
    }
  },
  
  async mounted() {
    await this.fetchData('olympiad');
  },

  methods: {
    async fetchData(type) {
      this.activeTab = type;
      this.$store.commit('setLoading', true);
      try {
        const res = await this.$http({
          url: this.$getAPI(`testResult.${type}`),
        params: {
          page: this.currentPage,
        }
      });
        this.resultList = res?.rows;
        this.lastPage = res?.lastPage;
        this.currentPage = res?.currentPage;
      } finally {
        this.$store.commit('setLoading', false);
      }
    },

    async changePage(page) {
      this.currentPage = page;
      await this.fetchData()
    }
  }
}