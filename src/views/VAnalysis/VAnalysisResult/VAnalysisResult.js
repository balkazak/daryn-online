import VSelect from "@/components/VCustomSelect";
import {mapGetters} from "vuex";
import { ProgressCircle } from 'vue-progress-circle'

export default {
    name: "VAnalysisResult",
    components: {
        VSelect,
        ProgressCircle
    },
    data() {
        return {
            fiftyPercent: 51
        }
    },
    computed: {
        ...mapGetters([
            'getProbabilityResult',
        ]),
        isRed() {
            return 'rgb(254, 82, 68)'
        },
        isGreen() {
            return 'rgb(46, 204, 113)'
        },
    },
    methods: {
        changeResult() {
            this.$emit('changeResult');
        }
    }
}
