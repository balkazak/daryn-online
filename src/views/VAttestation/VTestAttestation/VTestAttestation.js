import VRadioGroup from "components/VRadioGroup/VRadioGroup";
import VCheckboxGroup from "components/VCheckboxGroup/VCheckboxGroup";
import draggable from 'vuedraggable'
import { DragAndDrop } from '@/helpers/drag-and-drop';
import {isNumber} from "lodash";
export default {
  name: 'VTestAttestation',

  components: {
    VRadioGroup,
    VCheckboxGroup,
    draggable
  },

  directives: {
    DragAndDrop
  },

  data() {
    return {
      questions: [],
      timer: '',
      points: 0,
      currentQuestion: {},
      selectedAnswer: null,
      multipleSelected: [],
      showCorrect: false,
      matchRightQuestion: [],
      answerText: '',
      dragSrcEl: null,
      list: [
        {id: 1, title: '6666'},
        {id: 2, title: '7777'},
        {id: 3, title: '8888'}
      ],
      currentlyDragging: null,
      loggedEvent: '',
    }
  },

  created() {
    this.fetchQuestionsData();
    // let checkSessionTimeout = function () {
    //   let minutes = Math.abs((initialTime - new Date()) / 1000 / 60);
    //   if (minutes > 20) {
    //     setInterval(function () { location.href = 'Audit.aspx' }, 5000)
    //   }
    // };
    // setInterval(checkSessionTimeout, 1000);
  },

  watch: {
    isMatch() {
      setTimeout(() => {
        this.setHeight();
      }, 1000)
    }
  },

  computed: {
    currentQuestionTitle() {
      return this.currentQuestion.question || '';
    },
    audioLink() {
      return this.currentQuestion.audioLink;
    },
    isLastQuestion() {
      const lastIdx = this.questions?.length - 1;
      const idx = this.questions?.findIndex(el => el.questionId === this.currentQuestion.questionId)
      return idx === lastIdx;
    },
    getVariants() {
      return this.currentQuestion.variants;
    },
    isAnswered() {
      return this.currentQuestion.isAnswered;
    },
    isMultipleAnswer() {
      return this.currentQuestion.type === 'several_answer';
    },
    isSingleAnswer() {
      return this.currentQuestion.type === 'single_answer';
    },
    isOpenQuestion() {
      return this.currentQuestion.type === 'open_question';
    },
    isMatch() {
      return this.currentQuestion.type === 'match';
    },
    matchLeftQuestion() {
      return this.currentQuestion.leftSide;
    },
    userAnswersBlue() {
      return this.currentQuestion.userAnswersBlue || [];
    },
  },

  methods: {
    fetchQuestionsData() {
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('attestation.getQuestions', {
          id: this.$route.params.id
        })
      }).then(res => {
        if (!res.error) {
          this.setData(res);
        }
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setData(res) {
      if (res.leftSeconds !== 0) {
        this.setTimer(res.leftSeconds);
      } else {
        this.handleFinish();
      }
      if (!res.questions) return;
      // this.points = res.points;
      let array = Object.keys(res.questions)
          .map(function(key) {
            return res.questions[key];
          });
      this.questions = array

      this.setDefaultCurrentQuestion();
      this.togglePagination();
      this.setSelectedAnswer();
    },

    setDefaultCurrentQuestion() {
      this.currentQuestion = this.questions[0];
    },

    handleSelectQuestion(idx) {
      if (!this.questions[idx]) {
        this.handleFinish();
      } else {
        this.currentQuestion = this.questions[idx];
        this.setSelectedAnswer();
        this.togglePagination();
        if (this.currentQuestion.type === 'match') {
          setTimeout(() => this.setHeight(), 300);
        }
      }
    },

    togglePagination() {
      this.questions.forEach((el) => {
        // if (el.status.includes('info')) {
        //   const idx = el.status.search('info');
        //   el.status = el.status.slice(0, idx);
        // }
        if (el.questionId === this.currentQuestion.questionId) {
          el.status += ' info';
        }
      });
    },

    setSelectedAnswer() {
      this.multipleSelected = [];
      this.selectedAnswer = null;
      this.answerText = '';
      this.matchRightQuestion = this.currentQuestion.rightSide;
    },

    changeAnswer(answerId) {
        this.selectedAnswer = answerId;
        this.currentQuestion.userAnswersBlue = [answerId];
    },

    handleNextQuestion() {
      const index = this.questions.findIndex(el => el.questionId === this.currentQuestion.questionId);
      if (index !== -1) {
        const nextQuestionIdx = index + 1;
        this.handleSelectQuestion(nextQuestionIdx);
      }
    },

    async handleCheck() {
      if (this.selectedAnswer || this.multipleSelected?.length || this.answerText || this.matchRightQuestion?.length) {
        await this.sendRequest();
      } else {
        this.$notify({
          type: 'error',
          title: '',
          text: this.trans('general.choose_answer'),
        });
      }
    },

    handleFinish() {
      const userAttestationId = this.$route.params?.id;
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        url: this.$getAPI('attestation.finish', {
          userAttestationId
        }),
        method: 'POST'
      }).then(() => {
        this.$router.push({name: 'VResultAttestation', params: { id: userAttestationId }});
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    getParams() {
      if (this.isOpenQuestion) {
        return {
          type: 'open_question',
          open_question_answer: this.answerText
        }
      } else if (this.isMatch) {
        return {
          type: 'match',
          match_answers: [
            this.matchLeftQuestion.map(el => el.id),
            this.matchRightQuestion.map(el => el.id)
          ]
        }
      } else {
        return {
          answers: [this.selectedAnswer],
        }
      }
    },

    async sendRequest() {
      const userAttestationId = this.$route.params?.id;
      const questionId = this.currentQuestion.questionId;
      let params = this.getParams();
      this.$store.commit('setSpinnerLoader', true);
      this.$http({
        method: 'POST',
        url: this.$getAPI('attestation.sendAnswer', {
          userAttestationId,
          questionId
        }),
        data: params
      }).then((res) => {
        if (!res.error) {
          this.currentQuestion.isAnswered = true;
          this.points += isNumber(res.point) ? res.point : 0;
        }
      }).finally(() => {
        this.$store.commit('setSpinnerLoader', false);
      });
    },

    setTimer(seconds) {
      let timer;
      timer = setInterval(() => {
        if (seconds >= 0) {
          this.timer = this.getTimer(seconds--);
        } else {
          clearInterval(timer);
          this.handleFinish();
        }
      }, 1000);
    },

    getTimer(seconds) {
      if (seconds < 3600) {
        return new Date(seconds * 1000).toISOString().substr(14, 5)
      }
      return new Date(seconds * 1000).toISOString().substr(11, 8)
    },

    getStyle(question) {
      if (question.isAnswered) {
        return {
          cursor: 'default',
          background: '#4BB34B',
          borderColor: '#4BB34B',
          color: '#FFFFFF'
        }
      } else {
        return { cursor: 'pointer', }
      }
    },
    changeText(event) {
      event.target.style.height = 'auto';
      event.target.style.height = event.target.scrollHeight + 'px';
    },
    setHeight() {
      let tags = [];
      tags = document.getElementsByClassName('compare-test-item');
      let arr = Array.from(tags).map(tag => tag.clientHeight);
      let height = Math.max(...arr);
      Array.from(tags).forEach((el) => {
        el.style.height = height + 'px'
      });
    },

    handleDragStart(e) {
      e.target.style.opacity = '0.4';
      this.dragSrcEl = e.srcElement;
      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setData('text/html', e.target.innerHTML);
    },
    handleDragEnd(e) {
      e.target.style.opacity = '1';
      let items = document.querySelectorAll('.compare-answer');
      items.forEach(function (item) {
        item.classList.remove('over');
      });
    },
    handleDragOver(e) {
      if (e.preventDefault) {
        e.preventDefault();
      }
      return false;
    },
    handleDragEnter(e) {
      e.target.classList.add('over');
    },
    handleDragLeave(e) {
      e.target.classList.remove('over');
    },
    handleDrop(e) {
      e.stopPropagation();

      if (this.dragSrcEl !== e.srcElement) {
        this.dragSrcEl.innerHTML = e.target.innerHTML;
        e.target.innerHTML = e.dataTransfer.getData('text/html');
      }

      return false;
    },
    setHeightMatch() {
      let max_height = 0;
      const question = document.querySelectorAll('.compare-question');
      const answer = document.querySelectorAll('.compare-answer');
      const items = document.querySelectorAll('.compare-test-item');
      question.forEach((el, index) => {
        if (el.clientHeight > max_height) {
          max_height = el.clientHeight;
        }
        if (answer[index].clientHeight > max_height) {
          max_height = answer[index].clientHeight;
        }
      });
      items.forEach((el) => {
        el.style.height = max_height + 'px'
      });
    }

  },
}
